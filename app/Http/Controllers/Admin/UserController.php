<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\UserAdmin;
use App\Models\Role;
use App\Models\Orgao;

class UserController extends Controller
{
    protected $table = 'users';

    public function index()
    {
        $this->authorize('users_view');

        return view('admin.users.index');
    }


    public function json(Request $request)
    {
        $this->authorize('users_view');

        $datatablejson = new UserAdmin();
        return $datatablejson->dataTable($request);
    }


    public function create()
    {
        $this->authorize('users_create');
        return view('admin.users.edit',array('model'=>UserAdmin::class,'routeModel'=>['route' => ['users.store']],'validator'=>null));
    }


    public function store(Request $request)
    {
        //verifica permissao
        $this->authorize('users_create');
        //agrupa os campos enviados pelo request
        $fieldsFromRequest = $request->all();
        list($day,$month,$year) = explode("/",$fieldsFromRequest['nascimento']);
        $fieldsFromRequest['nascimento'] = "{$year}-{$month}-{$day}";
        //valida os campos enviados
        $validator = Validator::make($fieldsFromRequest, [
            'name' =>       'required|min:3|max:255',
            'email' =>      'required|email',
            'nascimento' => 'required|date',
            'cpf' =>        'required|min:11|max:50',

        ]);
        //se falhou validação, envia novamente para o formulário
        if ($validator->fails()) {
            return redirect()->route('users.create') ->withErrors($validator)->withInput();

        }
        //instancia o Model para executar a query CRUD
        $objModel = new UserAdmin();
        //remove os campos extras enviados pelo formulario e deixa apenas os campos que coincidem com as colunas da tabela
        $fieldsToUpdate = array_intersect_key($fieldsFromRequest,array_flip($objModel->getConnection()->getSchemaBuilder()->getColumnListing($objModel->getTable())) );
        //seta o status
        $fieldsToUpdate['ativo'] = isset($fieldsToUpdate['ativo']);
        //seta a senha inicial do usuário
        $fieldsToUpdate['password'] = bcrypt($request->input('cpf'));
        //faz a ação CRUD
        $objModelInserted = $objModel->updateOrCreate(array('id'=>0),$fieldsToUpdate);


        //envia para a listagem com mensagem de sucesso
        return redirect()->route('users.edit',$objModelInserted->id) ->with('msgSuccess','Incluído com sucesso. ID: '.$objModelInserted->id.'. ');
    }


    public function edit(Request $request,$id)
    {
        //verifica permissao
        $this->authorize('users_update');
        $user = UserAdmin::find($id);
        return view('admin.users.edit',array('model'=>$user,'routeModel'=>['route' => ['users.update', $id]],'validator'=>null));
    }


    public function update(Request $request, $id)
    {
        //verifica permissao
        $this->authorize('users_update');
        //agrupa os campos enviados pelo request
        $fieldsFromRequest = $request->all();
        list($day,$month,$year) = explode("/",$fieldsFromRequest['nascimento']);
        $fieldsFromRequest['nascimento'] = "{$year}-{$month}-{$day}";
        //valida os campos enviados
        $validator = Validator::make($fieldsFromRequest, [
            'name' =>       'required|min:3|max:255',
            'email' =>      'required|email',
            'nascimento' => 'required|date',
            'cpf' =>        'required|min:11|max:50',
        ]);
        //se falhou validação, envia novamente para o formulário
        if ($validator->fails()) {
            return redirect()->route('users.edit',$id) ->withErrors($validator)->withInput();
        }
        //instancia o Model para executar a query CRUD
        $objModel = new UserAdmin();
        //remove os campos extras enviados pelo formulario e deixa apenas os campos que coincidem com as colunas da tabela
        $fieldsToUpdate = array_intersect_key($request->all(),array_flip($objModel->getConnection()->getSchemaBuilder()->getColumnListing($objModel->getTable())) );
        //seta o status
        $fieldsToUpdate['ativo'] = isset($fieldsToUpdate['ativo']);
        //faz a ação CRUD
        UserAdmin::updateOrCreate(array('id'=>$id),$fieldsToUpdate);
        //envia para a listagem com mensagem de sucesso
        return redirect()->route('users.index') ->with('msgSuccess','Alterado com sucesso. ID: '.$id.'. ');
    }


}
