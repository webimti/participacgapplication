@extends('adminlte::page')


@section('content_header')
    <h1>Erro de permissão</h1>
@stop

@section('content')
    <div class="box">
        <div class="alert alert-danger" role="alert">Você não tem permissão para acessar este recurso.</div>
    </div>

@stop



