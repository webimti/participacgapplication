<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\User;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Model' => 'App\Policies\ModelPolicy',
        //\App\User::class => \App\Policies\UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate );

        //recupera todas as permissoes existentes no sistema (users_view, users_edit, etc) que estejam vinculadas a algum papel
        $permissions = Permission::with('roles')->get();

        // cada permissão é passada para a função hasPermission da classe User Authenticatable
        foreach($permissions as $permission){
            $gate->define(
                $permission->name,
                function(User $user) use ($permission) {
                    return $user->hasPermission($permission);
                }
            );
        }

        //verifica se é superAdmin
        $gate->before(function(User $user,$ability){
            if( $user->hasAnyRoles('ADM') )
                return true;
        });
    }


}
