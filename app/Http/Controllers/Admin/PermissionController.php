<?php

namespace App\Http\Controllers\Admin;

use App\Models\Permission;
use App\Models\Role;
use App\Models\Permission_Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;


class PermissionController extends Controller
{
    public function index()
    {
        $this->authorize('adm');

        return view('admin.permission.index');
    }

    public function json(Request $request)
    {
        $this->authorize('adm');

        $datatablejson = new Permission();
        return $datatablejson->dataTable($request);
    }

    public function create()
    {
        $this->authorize('adm');
        return view('admin.permission.edit',array('model'=>Permission::class,'routeModel'=>['route' => ['permissions.store']],'validator'=>null));
    }

    public function store(Request $request)
    {
        //verifica permissao
        $this->authorize('adm');
        //valida os campos enviados
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'label' => 'required|min:3|max:200',
        ]);
        //se falhou validação, envia novamente para o formulário
        if ($validator->fails()) {
            return redirect()->route('permissions.create') ->withErrors($validator)->withInput();
        }
        //instancia o Model para executar a query CRUD
        $objModel = new Permission();
        //remove os campos extras enviados pelo formulario e deixa apenas os campos que coincidem com as colunas da tabela
        $fieldsToUpdate = array_intersect_key($request->all(),array_flip($objModel->getConnection()->getSchemaBuilder()->getColumnListing($objModel->getTable())) );
        //seta o status
        $fieldsToUpdate['ativo'] = isset($fieldsToUpdate['ativo']);
        //faz a ação CRUD
        //print_r($fieldsToUpdate);exit;
        $objModelInserted = $objModel->updateOrCreate(array('id'=>0),$fieldsToUpdate);
        //envia para a listagem com mensagem de sucesso
        return redirect()->route('permissions.index') ->with('msgSuccess','Alterado com sucesso. ID: '.$objModelInserted->id.'. ');
    }

    public function edit($id)
    {
        //verifica permissao
        $this->authorize('adm');
        return view('admin.permission.edit',array('model'=>Permission::find($id),'routeModel'=>['route' => ['permissions.update', $id]],'validator'=>null));
    }


    public function update(Request $request, $id)
    {
        //verifica permissao
        $this->authorize('adm');
        //valida os campos enviados
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'label' => 'required|min:3|max:200',
        ]);
        //se falhou validação, envia novamente para o formulário
        if ($validator->fails()) {
            return redirect()->route('permissions.edit',$id) ->withErrors($validator)->withInput();
        }
        //instancia o Model para executar a query CRUD
        $objModel = new Permission();
        //remove os campos extras enviados pelo formulario e deixa apenas os campos que coincidem com as colunas da tabela
        $fieldsToUpdate = array_intersect_key($request->all(),array_flip($objModel->getConnection()->getSchemaBuilder()->getColumnListing($objModel->getTable())) );
        //seta o status
        $fieldsToUpdate['ativo'] = isset($fieldsToUpdate['ativo']);
        //faz a ação CRUD
        Permission::updateOrCreate(array('id'=>$id),$fieldsToUpdate);
        //envia para a listagem com mensagem de sucesso
        return redirect()->route('permissions.index') ->with('msgSuccess','Alterado com sucesso. ID: '.$id.'. ');
    }

    public function permissionroleindex(){
        $this->authorize('adm');
        return view('admin.permission.permissionrole',['roles'=>Role::class, 'permissions'=>Permission::class, 'permissionsroles'=>Permission_Role::class,'setado'=>false]);
    }
    public function permissionroleupdate(Request $request){
        $this->authorize('adm');
        Permission_Role::where('permission_id','>',0)->delete();
        foreach ($request->all()['perm_role'] as $permissao_id=>$roles){
            foreach ($roles as $role_id=>$role){
                Permission_Role::updateOrCreate(array('permission_id'=>0),['permission_id'=>$permissao_id,'role_id'=>$role_id]);
            }
        }
        return view('admin.permission.permissionrole',['roles'=>Role::class, 'permissions'=>Permission::class, 'permissionsroles'=>Permission_Role::class,'setado'=>false])->with('msgSuccess','Permissões e Papéis alterados com sucesso. ');;
    }
}
