<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


/**
 * Class BaseModel
 * Contém todas as customizações comund à todas as Models do sistema
 * @package App\Models
 */
class BaseModel extends Model
{
    /**
     * Retorna o tipo da coluna informada
     * @param string $coluna Nome da coluna para se obter o tipo
     * @return string
     */
    public function colunaTipo($coluna='id'){
        //return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
        return $this->getConnection()->getSchemaBuilder()->getColumnType($this->getTable(),$coluna);
    }

    /**
     * Retorna JSON formatado para o Datatables
     * Faz as searchs, order by, etc vindas do datatables
     * @param $request  - recebe os dados da requisicao
     * @param $relacao - (opcional. Recebe array com par de relacao ['campo_id'=>['model'=>Model::class, 'tabela'=>'nome_tabela', 'campo_view'=>'campo_visivel', 'campo_json'=>'nome_a_enviar_ao_json']])
     * @return json datatables format
     */
    public function dataTable(Request $request,$relacao=null){
        //inicia o objeto da query
        $query = $this::query();

        //print_r($request->all());exit;


        /*Filtros*/
        //recupera as colunas do datatable
        $colunas = $request->input('columns');
        foreach ($colunas as $coluna) {
            global $colunaValor;
            $colunaValor = $coluna['search']['value'];


            //inclui a coluna a ser retornada na consulta
            if( $relacao!=null && array_key_exists($coluna['data'],$relacao)){
                $query->addSelect($relacao[$coluna['data']]['tabela'].".".$relacao[$coluna['data']]['campo_view']." as ".$relacao[$coluna['data']]['campo_json']);
            }else{
                $query->addSelect($this->getTable().".".$coluna['data']);

            }

            //se enviou filtro para a coluna
            if($coluna['searchable']=='true' && !empty($colunaValor)){
                //se a coluna é string
                if( $this->colunaTipo($coluna['data']) == 'string')
                {
                    $query->whereRaw("LOWER(".$this->getTable().".".$coluna['data'].") LIKE LOWER('%".$colunaValor."%')");
                }

                //se a coluna é integer
                elseif( $this->colunaTipo($coluna['data']) == 'integer')
                {
                    $query->where($this->getTable().".".$coluna['data'],   $colunaValor );
                }

                //se a coluna é boolean
                elseif( $this->colunaTipo($coluna['data']) == 'boolean')
                {
                    global $nomeColuna;
                    $nomeColuna = $coluna['data'];

                    if($colunaValor=='ativo') {
                        $query->where($this->getTable().".".$coluna['data'], 1);
                    }
                    else if($colunaValor=='todos'){
                        $query->where(function($query){
                            global $nomeColuna;
                            return $query->where($this->getTable().".".$nomeColuna, 1)->orWhere($this->getTable().".".$nomeColuna, '<>',1);
                        }) ;
                    }
                    else {
                        $query->where($this->getTable().".".$coluna['data'], '<>', 1);
                    }
                }
                //se a coluna é Date
                elseif( $this->colunaTipo($coluna['data']) == 'date' && strpos($colunaValor,"1919")==FALSE)
                {
                    global $nomeColuna;
                    $nomeColuna = $coluna['data'];
                    global $colunaValor;
                    $colunaValor = explode(" - ",$colunaValor);
                    if(sizeof($colunaValor)>1) {
                        $query->where(function ($query) {
                            global $nomeColuna;
                            return
                                $query->whereNull($this->getTable() . "." . $nomeColuna)->orWhere(function ($query) {
                                    global $nomeColuna;
                                    global $colunaValor;
                                    return
                                        $query->where($this->getTable() . "." . $nomeColuna, '>=', $colunaValor[0])
                                            ->where($this->getTable() . "." . $nomeColuna, '<=', $colunaValor[1]);
                                });
                        });
                    }else{
                        $query->where($this->getTable() . "." . $nomeColuna, '=', $colunaValor[0]);
                    }
                }
                //se a coluna é relaçao
                elseif( $relacao!=null && array_key_exists($coluna['data'],$relacao))
                {
                    $query->where($this->getTable().".".$coluna['data'],   $colunaValor );
                }
            }//if coluna serachable
        }//foreach colunas


        /*Verifica ordenação*/
        if ($request->filled('order') && !empty($request->input('order')[0])) {
            $query->orderBy(       $this->getTable().".".$request->input('columns')[$request->input('order')[0]['column']]['data'] , $request->input('order')[0]['dir']);
        }



        if($relacao!=null){
            foreach ($relacao as $campo=>$rel) {
                $query->join($rel['tabela'], $campo, '=', $rel['tabela'].'.id');
            }
        }


        /*total de registros com os filtros*/
        $recordsFiltered = $query->get()->count();


        /*Paginação*/
        $query->offset($request->input('start'))->limit($request->input('length'));



        /*total de registros filtrados após a paginação*/
        $recordsTotal = $query->get()->count();

        /*prepara o objeto no formato datatables*/
        $requestArray = $request->all();
        $objetoFinalDataTables['draw'] = $requestArray['draw'];
        $objetoFinalDataTables['recordsTotal'] = $recordsTotal;
        $objetoFinalDataTables['recordsFiltered'] = $recordsFiltered;
        $objetoFinalDataTables['input'] = $requestArray;
        //coloca toda a consulta customizada dentro do objeto datatables
        $objetoFinalDataTables['data'] = $query->get()->toArray();

        return json_encode($objetoFinalDataTables);
    }
}
