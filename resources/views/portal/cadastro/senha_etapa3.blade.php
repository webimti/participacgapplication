@extends('layouts.portal')


@section('content')
    <style>
        input{text-transform: uppercase;}
        .help-block{color: red !important;}
    </style>

    <div class="container inner-nav">
        <div class="row">
            <div class="col-md-12">
                <div class="blue-bar"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="wrapper">

                </div>
                <div class="area-title">
                    <h2>REINICIAR <strong>SENHA</strong></h2>
                </div>
            </div>
        </div>
    </div>
    <!--/inner-nav-->
    <!--forms-->
    <div class="container inner-pages">
        <!--inner-title-->
        <div class="row inner-title">
            <div class="col-md-12">
                <h3><i class="glyphicon glyphicon-user"></i> Confirme seus <strong>Dados</strong></h3>
                <p>Preencha todos os campos para confirmar sua identidade</p>
                <p><span class="text-danger">*</span> Dados obrigatórios</p>
                <hr>
            </div>
        </div>
        <!--/inner-title-->

        {!! Form::open(['route' => ['portal.senha.store',3],'onsubmit'=>'return validaFormCadastro(this)']) !!}
        <input type="hidden" name="_method" value="put" />
        <input type="hidden" name="id" value="{{$usuario['name']}}" />
        <div class="row">
            <div class="col-md-12">
                    <div class="row stitle">
                        <div class="col-md-12">
                            <h4>Dados pessoais</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Nome</label>
                                {!! Form::text('name_ex',$usuario['name'],['class' => 'form-control','required'=>true,'disabled'=>true]) !!}
                                @if ($errors->has('name'))
                                    <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                @endif
                                {!! Form::hidden('name',$usuario['name']) !!}
                                {{--<input type="text" class="form-control" name="name2" value="{{$nome_completo}}" disabled required>--}}
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label>Data de Nascimento</label>
                                {!! Form::text('nascimento_ex',date('d/m/Y',strtotime($usuario['nascimento'])),['class' => 'form-control','required'=>true,'disabled'=>true]) !!}
                                @if ($errors->has('nascimento'))
                                    <span class="help-block"><strong>{{ $errors->first('nascimento') }}</strong></span>
                                @endif
                                {{--<input type="text" class="form-control" name="nascimento_ex" value="{{date('d/m/Y',strtotime($nascimento))}}" disabled required>--}}
                                {!! Form::hidden('nascimento',$usuario['nascimento']) !!}
                                {{--<input type="hidden" name="nascimento" value="{{$nascimento}}">--}}
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label>Sexo</label>
                                {!! Form::text('sexo_ex',($usuario['sexo']=='m')?'MASCULINO':'FEMININO',['class' => 'form-control','required'=>true,'disabled'=>true]) !!}
                                @if ($errors->has('sexo'))
                                    <span class="help-block"><strong>{{ $errors->first('sexo') }}</strong></span>
                                @endif
                                {!! Form::hidden('sexo',$usuario['sexo']) !!}

                                {{--<input type="text" class="form-control" name="sexo_ex" value="{{($sexo=='m')?'MASCULINO':'FEMININO'}}" disabled required>--}}
                                {{--<input type="hidden" name="sexo" value="{{$sexo}}">--}}
                            </div>
                        </div>
                    </div>

                    <div class="row stitle">
                        <div class="col-md-12">
                            <h4>Confirme os dados abaixo</h4>
                        </div>
                    </div>
                    <div class="row">

                        @isset($usuario['cpf'])
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>CPF do Cidadão <span class="text-danger">*</span></label>
                                {!! Form::text('cpf',null,['id' => 'cpf','class' => 'form-control','required'=>true]) !!}
                                @if ($errors->has('cpf'))
                                    <span class="help-block"><strong>{{ $errors->first('cpf') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        @endisset

                        @isset($usuario['email'])
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>E-mail <span class="text-danger">*</span></label>
                                    {!! Form::email('email',null,['id' => 'email','class' => 'form-control','required'=>true]) !!}
                                    @if ($errors->has('email'))
                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                        @endisset

                        @isset($usuario['responsavel_cpf'])
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>CPF do Responsável<span class="text-danger">*</span></label>
                                    {!! Form::text('responsavel_cpf',null,['id' => 'responsavel_cpf','class' => 'form-control','required'=>true]) !!}
                                    @if ($errors->has('responsavel_cpf'))
                                        <span class="help-block"><strong>{{ $errors->first('responsavel_cpf') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                        @endisset

                    </div>


                    <div class="row stitle">
                        <div class="col-md-12">
                            <h4>Digite sua nova senha</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Senha <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    {!! Form::password('senha',['id' => 'senha','class' => 'form-control','required'=>true]) !!}
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-eye-open" title="Mostrar senha"></i></span>
                                </div>
                                @if ($errors->has('senha'))
                                    <span class="help-block"><strong>{{ $errors->first('senha') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Confirmar senha <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    {!! Form::password('senha_confirmation',['id' => 'senha_confirmation','class' => 'form-control','required'=>true]) !!}
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-eye-close" title="Mostrar senha"></i></span>
                                </div>
                                @if ($errors->has('senha_confirmation'))
                                    <span class="help-block"><strong>{{ $errors->first('senha_confirmation') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <hr>
                                <p class="help-block">Confira os dados e clique no botão abaixo</p>
                                <input type="submit" class="btn btn-lg btn-primary" value="ALTERAR SENHA">
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <hr>
                                <p class="help-block">Já está cadastrado?</p>
                                <a href="{{route('portal.home')}}" class="btn btn-lg btn-primary">FAZER LOGIN</a>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!--/forms-->
    <!--separator-->
    <div class="container">
        <div class="row separator">
            <div class="col-md-12">
                <div class="wrapper"></div>
            </div>
        </div>
    </div>
    <!--/separator-->
@stop


@section('js')
    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/Inputmask-4.x/dist/jquery.inputmask.bundle.js') !!}"></script>
    <script>
        function validaFormCadastro(form){
            var erros=[];


            @isset($usuario['cpf'])
            if(form.cpf.value!="" && !validaCPF(form.cpf.value)) erros.push("O CPF do Beneficiário é inválido.");
            @endisset

            @isset($usuario['responsavel_cpf'])
            if(form.responsavel_cpf.value=="" || !validaCPF(form.responsavel_cpf.value)) erros.push("O CPF do Responsável é inválido.");
            @endisset

            if(form.senha.value.length<6 || form.senha_confirmation.value.length<6) erros.push("A senha deve conter no mínimo 6 caracteres.");
            if(form.senha.value!="" && form.senha_confirmation.value!= "" && form.senha.value != form.senha_confirmation.value) erros.push("A Senha e a confirmação não coincidem.");

            if(erros.length>0){
                alerta('Existem erros',erros);
                return false;
            }
            return true;
        }



        $(function () {
            $('#celular').inputmask({"mask": "(99) 99999-9999"});
            $('#outrofone').inputmask({"mask": "(99) 99999-9999"});
            $('#responsavel_fone').inputmask({"mask": "(99) 99999-9999"});
            $('#responsavel_cpf').inputmask({"mask": "999.999.999-99"});
            $('#cpf').inputmask({"mask": "999.999.999-99"});
        });
    </script>

@stop