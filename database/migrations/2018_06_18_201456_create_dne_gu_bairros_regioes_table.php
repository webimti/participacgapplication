<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDneGuBairrosRegioesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dne_gu_bairros_regioes', function (Blueprint $table) {
            $table->integer('chave_bai_dne')->unsigned();
            $table->integer('chave_reg')->unsigned();

            $table->foreign('chave_bai_dne')
                ->references('chave_bai_dne')
                ->on('dne_gu_bairros')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('chave_reg')
                ->references('chave_reg')
                ->on('dne_gu_regioes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dne_gu_bairros_regioes');
    }
}
