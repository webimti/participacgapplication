<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends BaseModel
{
    protected $fillable = ['name','label','ativo','regional'];

    /**
     * Retorna todas as regras válidas (ativo=true) e que não seja adm (pois tem privilegio especial)
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function habilitados(){
        return Role::query()->whereRaw("LOWER(name) NOT LIKE LOWER('%adm%')")->where('ativo',1)->get();
    }

    /**
     * Retorna todas as regras válidas (ativo=true), inclusive ADM
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function habilitadosAll(){
        return Role::query()->where('ativo',1)->orderBy('name')->get();
    }
}
