@inject('request', 'Illuminate\Http\Request')

@section('css')
    <link rel="stylesheet" href="{!! asset('vendor/adminlte/plugins/daterangepicker-master/daterangepicker.css') !!}">
@stop




{!! Form::open(['route' => $rota]) !!}

<input type="hidden" name="_method" value="put" />
<input type="hidden" name="nascimento" value="{{$nascimento}}">
<input type="hidden" name="sexo" value="{{$sexo}}">

<div class="form-group">
    <label>Data de Nascimento</label>
    <h4>
        @php
            echo date("d/m/Y",strtotime($nascimento));
        @endphp
    </h4>
</div>
<div class="form-group">
    <label>Sexo</label>
    <h4>
        @switch($sexo)
        @case('m')
            Masculino
            @break
        @case('f')
            Feminino
            @break
        @endswitch
    </h4>
</div>
<div class="form-group">
    <label>Nome Completo (conforme RG)</label>
        <select name="name" id="nome_completo" class="form-control" ></select>
</div>
<button id="btn_prosseguir" class="btn btn-success btn-sm hide" type="submit">Proseguir >></button>
{!! Form::close() !!}


<style>
    .select2-container{z-index: 9999991 !important; color:red!important; }
    .select2-container .selection{z-index: 9999992 !important;}
    .select2-container .selection .select2-selection{z-index: 9999993 !important;}
    .select2-container .selection .select2-selection .select2-selection__rendered{z-index: 9999994 !important;}
    .select2-container .selection .select2-selection .select2-selection__rendered .select2-search{z-index: 9999995 !important;}
    .select2-container .selection .select2-selection .select2-selection__rendered .select2-search .select2-search__field{z-index: 9999996 !important;}
    select2-container input{text-transform: uppercase!important;}
</style>

@section('js')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{!! asset('js/select2-pt-BR.js') !!}"></script>

    <link rel="stylesheet" href="{!! asset('vendor/adminlte/plugins/daterangepicker-master/daterangepicker.css') !!}">
    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/daterangepicker-master/moment.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/daterangepicker-master/daterangepicker.js') !!}"></script>


    <script>
        $(function () {


             $('#nome_completo').select2({
                 language: "pt-BR",
                minimumInputLength: 3,
                 allowClear: true,
                 placeholder: "Nome Completo",
                ajax: {
                    url: '{{route('portal.users.json')}}',
                    dataType: 'json',
                    cache: false,
                    delay: 1000,
                    data: function (params) {
                        params.page = (params.page) ? params.page:0;
                        var query = {
                            columns: [
                                {
                                    data: 'id',
                                    searchable : false,
                                    orderable : false,
                                    'search': {
                                        value: ''
                                    }
                                },{
                                    data: 'name',
                                    searchable : true,
                                    orderable : false,
                                    'search': {
                                        value: params.term?params.term:''
                                    }
                                },{
                                    data: 'nascimento',
                                    searchable : true,
                                    orderable : false,
                                    'search': {
                                        value: '{{$nascimento}}'
                                    }
                                },{
                                    data: 'sexo',
                                    searchable : true,
                                    orderable : false,
                                    'search': {
                                        value: '{{$sexo}}'
                                    }
                                }
                            ],
                            order: [{
                                column: 1,
                                dir : 'asc'
                            }],
                            draw : 1,
                            start :  (params.page*50),
                            length : 500,
                            '_': Math.random()
                        };
                        return query;
                    },
                    processResults: function (data,params) {
                        var arr = Object.values(data.data);

                        @if($request->route()->getName()!="portal.senha.etapas")
                        arr.push({id: $('.select2-search__field')[0].value , text: 'Cadastrar: '+$('.select2-search__field')[0].value});
                        @endif

                        return {
                            results: dat = JSON.parse(JSON.stringify(arr).split('name').join('text')),
                            pagination: {
                                more: (params.page*50+data.recordsTotal)  < data.recordsFiltered
                            }
                        };
                    }
                }
            }).on('select2:select', function (e) {
                var data = e.params.data;
                console.log(data);
                 $('#btn_prosseguir').removeClass('hide');
            });

        });
    </script>
@stop
