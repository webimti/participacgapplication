<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Cria a tabela roles (papeis de usuários)
         */
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name',50);
            $table->string('label',200);
            $table->boolean('ativo')->default(true);
        });

        /**
         * Cria a ligação entre usuários e papeis
         */
        Schema::create('role_user', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();


            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->integer('orgao_id')->unsigned()->nullable();


            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onDelete('cascade');

            $table->foreign('orgao_id')
                ->references('id')
                ->on('orgaos')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
