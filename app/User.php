<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Permission;
use App\Models\Role;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Retorna todas as Roles (papeis) vinculados ao usuário atual
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(){
        return $this->belongsToMany(Role::class);
    }

    /**
     * Passa uma permissão para verificar se o usuario atual tel algum papel (role) vinculado a essa permissao
     * @param \App\Permission $permission
     * @return mixed
     */
    public function hasPermission(Permission $permission){
         return $this->hasAnyRoles($permission->roles->pluck('name')->toArray());
    }

    /**
     * recebe uma permissão e verifica se o usuário está em algum papel que contenha essa permissão.
     * @param $roles
     * @return mixed
     */
    public function hasAnyRoles($roles){

        if(is_array($roles) || is_object($roles)){
            foreach ($roles as $role){
                //se o role passado  é igual a algum role que o usuario possui
                if(in_array($role,$this->roles->pluck('name')->toArray()  )){
                    return true;
                }
            }
        }

        return in_array($roles,$this->roles->pluck('name')->toArray()  );
    }
}
