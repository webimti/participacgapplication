<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Validator;


class RoleController extends Controller
{

    public function index()
    {
        $this->authorize('adm');

        return view('admin.role.index');
    }

    public function json(Request $request)
    {
        $this->authorize('adm');

        $datatablejson = new Role();
        return $datatablejson->dataTable($request);
    }

    public function create()
    {
        $this->authorize('adm');
        return view('admin.role.edit',array('model'=>Role::class,'routeModel'=>['route' => ['roles.store']],'validator'=>null));
    }

    public function store(Request $request)
    {
        //verifica permissao
        $this->authorize('adm');
        //valida os campos enviados
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'label' => 'required|min:3|max:200',
        ]);
        //se falhou validação, envia novamente para o formulário
        if ($validator->fails()) {
            return redirect()->route('roles.create') ->withErrors($validator)->withInput();
        }
        //instancia o Model para executar a query CRUD
        $objModel = new Role();
        //remove os campos extras enviados pelo formulario e deixa apenas os campos que coincidem com as colunas da tabela
        $fieldsToUpdate = array_intersect_key($request->all(),array_flip($objModel->getConnection()->getSchemaBuilder()->getColumnListing($objModel->getTable())) );
        //seta o status
        $fieldsToUpdate['ativo'] = isset($fieldsToUpdate['ativo']);
        //faz a ação CRUD
        $objModelInserted = $objModel->updateOrCreate(array('id'=>0),$fieldsToUpdate);
        //envia para a listagem com mensagem de sucesso
        return redirect()->route('roles.index') ->with('msgSuccess','Alterado com sucesso. ID: '.$objModelInserted->id.'. ');
    }

    public function edit($id)
    {
        //verifica permissao
        $this->authorize('adm');
        return view('admin.role.edit',array('model'=>Role::find($id),'routeModel'=>['route' => ['roles.update', $id]],'validator'=>null));
    }


    public function update(Request $request, $id)
    {
        //verifica permissao
        $this->authorize('adm');
        //valida os campos enviados
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'label' => 'required|min:3|max:200',
        ]);
        //se falhou validação, envia novamente para o formulário
        if ($validator->fails()) {
            return redirect()->route('roles.edit',$id) ->withErrors($validator)->withInput();
        }
        //instancia o Model para executar a query CRUD
        $objModel = new Role();
        //remove os campos extras enviados pelo formulario e deixa apenas os campos que coincidem com as colunas da tabela
        $fieldsToUpdate = array_intersect_key($request->all(),array_flip($objModel->getConnection()->getSchemaBuilder()->getColumnListing($objModel->getTable())) );
        //seta o status
        $fieldsToUpdate['ativo'] = isset($fieldsToUpdate['ativo']);
        $fieldsToUpdate['regional'] = isset($fieldsToUpdate['regional']);
        //faz a ação CRUD
        Role::updateOrCreate(array('id'=>$id),$fieldsToUpdate);
        //envia para a listagem com mensagem de sucesso
        return redirect()->route('roles.index') ->with('msgSuccess','Alterado com sucesso. ID: '.$id.'. ');
    }
}
