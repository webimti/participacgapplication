@extends('layouts.portal')

@php $TITLE = ['MEUS ','DADOS'] @endphp

@section('content')
    <style>
        input{text-transform: uppercase;}
        .help-block{color: red !important;}
    </style>

    @include('portal.home.topo_logado')

    <!--forms-->
    <div class="container inner-pages">
        <!--inner-title-->
        <div class="row inner-title">
            <div class="col-md-12">
                <h3><i class="glyphicon glyphicon-user"></i> Atualização <strong>cadastral</strong></h3>
                <p>Mantenha seus dados sempre atualizados</p>
                <p><span class="text-danger">*</span> Dados obrigatórios</p>
                <hr>
            </div>
        </div>
        <!--/inner-title-->

        {!! Form::model($usuario,['route' => 'portal.painel.dados.update','onsubmit'=>'return validaFormCadastro(this)']) !!}
        <div class="row">
            <div class="col-md-12">
                    <div class="row stitle">
                        <div class="col-md-12">
                            <h4>Dados pessoais</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Nome</label>
                                {!! Form::text('name',null,['class' => 'form-control','required'=>true,'disabled'=>true]) !!}
                                @if ($errors->has('name'))
                                    <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label>Data de Nascimento</label>
                                {!! Form::text('nascimento',date("d/m/Y",strtotime($usuario->nascimento)),['class' => 'form-control','required'=>true,'disabled'=>true]) !!}
                                @if ($errors->has('nascimento'))
                                    <span class="help-block"><strong>{{ $errors->first('nascimento') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label>Sexo</label>
                                {!! Form::text('sexo_ex',(@$sexo=='m')?'MASCULINO':'FEMININO',['class' => 'form-control','required'=>true,'disabled'=>true]) !!}
                                @if ($errors->has('sexo'))
                                    <span class="help-block"><strong>{{ $errors->first('sexo') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-8">
                            <div class="form-group">
                                <label>Celular</label>
                                {!! Form::text('celular',null,['id' => 'celular','class' => 'form-control']) !!}
                                @if ($errors->has('celular'))
                                    <span class="help-block"><strong>{{ $errors->first('celular') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-8">
                            <div class="form-group">
                                <label>Outro telefone</label>
                                {!! Form::text('outrofone',null,['id' => 'outrofone','class' => 'form-control']) !!}
                                @if ($errors->has('outrofone'))
                                    <span class="help-block"><strong>{{ $errors->first('outrofone') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Possui algum problema de saúde?</label>
                                {!! Form::text('saude',null,['class' => 'form-control','placeholder' => 'Caso possua, Informe aqui']) !!}
                                @if ($errors->has('saude'))
                                    <span class="help-block"><strong>{{ $errors->first('saude') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>

                    @isset($usuario->responsavel_nome)
                        @if(empty($usuario->responsavel_nome))
                        @else
                            {!! Form::hidden('responsavel','1') !!}

                            <div class="row stitle">
                        <div class="col-md-12">
                            <h4>Dados do Responsável</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Nome do Responsável <span class="text-danger">*</span></label>
                                {!! Form::text('responsavel_nome',null,['id' => 'responsavel_nome','class' => 'form-control','placeholder' => 'Nome do Responsável','required'=>true]) !!}
                                @if ($errors->has('responsavel_nome'))
                                    <span class="help-block"><strong>{{ $errors->first('responsavel_nome') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-8">
                            <div class="form-group">
                                <label>CPF <span class="text-danger">*</span></label>
                                {!! Form::text('responsavel_cpf',null,['id' => 'responsavel_cpf','class' => 'form-control','required'=>true]) !!}
                                @if ($errors->has('responsavel_cpf'))
                                    <span class="help-block"><strong>{{ $errors->first('responsavel_cpf') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-8">
                            <div class="form-group">
                                <label>Telefone <span class="text-danger">*</span></label>
                                {!! Form::text('responsavel_fone',null,['id' => 'responsavel_fone','class' => 'form-control','required'=>true]) !!}
                                @if ($errors->has('responsavel_fone'))
                                    <span class="help-block"><strong>{{ $errors->first('responsavel_fone') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>
                        @endif
                    @endisset


                    <div class="row stitle">
                        <div class="col-md-12">
                            <h4>Dados de acesso</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label>CPF <span class="text-danger">*</span></label>
                                {!! Form::text('cpf',null,['id' => 'cpf','class' => 'form-control','disabled'=>true,'required'=>true]) !!}
                                @if ($errors->has('cpf'))
                                    <span class="help-block"><strong>{{ $errors->first('cpf') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label>E-mail </label>
                                {!! Form::email('email',null,['id' => 'email','class' => 'form-control']) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label>Senha <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    {!! Form::password('senha',['id' => 'senha','class' => 'form-control']) !!}
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-eye-open" title="Mostrar senha"></i></span>
                                </div>
                                @if ($errors->has('senha'))
                                    <span class="help-block"><strong>{{ $errors->first('senha') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label>Confirmar senha <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    {!! Form::password('senha_confirmation',['id' => 'senha_confirmation','class' => 'form-control']) !!}
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-eye-close" title="Mostrar senha"></i></span>
                                </div>
                                @if ($errors->has('senha_confirmation'))
                                    <span class="help-block"><strong>{{ $errors->first('senha_confirmation') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/row 7-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <hr>
                                <p class="help-block">Confira os dados e clique no botão abaixo</p>
                                <input type="submit" class="btn btn-lg btn-primary" value="ALTERAR CADASTRO">
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!--/forms-->
    <!--separator-->
    <div class="container">
        <div class="row separator">
            <div class="col-md-12">
                <div class="wrapper"></div>
            </div>
        </div>
    </div>
    <!--/separator-->
@stop


@section('js')
    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/Inputmask-4.x/dist/jquery.inputmask.bundle.js') !!}"></script>
    <script>
        function validaFormCadastro(form){
            var erros=[];
            if(form.celular.value=="" && form.outrofone.value=="") erros.push("Informe pelo menos um número de telefone.");
            @isset($usuario->responsavel_nome)
                @if(empty($usuario->responsavel_nome))
                @else
            if(form.responsavel_nome.value=="") erros.push("Informe o Nome do Responsável.");
            if(form.responsavel_cpf.value=="" || !validaCPF(form.responsavel_cpf.value)) erros.push("O CPF do Responsável é inválido.");
            if(form.responsavel_fone.value=="") erros.push("Informe o Telefone do Responsável.");
                @endif
            @endisset
            if(form.cpf.value!="" && !validaCPF(form.cpf.value)) erros.push("O CPF do Beneficiário é inválido.");
            if((form.senha.value.length>0 && form.senha.value.length<6) || (form.senha_confirmation.value.length>0 && form.senha_confirmation.value.length<6)) erros.push("A senha deve conter no mínimo 6 caracteres.");
            if((form.senha.value!="" || form.senha_confirmation.value!= "") && form.senha.value != form.senha_confirmation.value) erros.push("A Senha e a confirmação não coincidem.");

            if(erros.length>0){
                alerta('Existem erros',erros);
                return false;
            }
            return true;
        }



        $(function () {
            $('#celular').inputmask({"mask": "(99) 99999-9999"});
            $('#outrofone').inputmask({"mask": "(99) 99999-9999"});
            $('#responsavel_fone').inputmask({"mask": "(99) 99999-9999"});
            $('#responsavel_cpf').inputmask({"mask": "999.999.999-99"});
            $('#cpf').inputmask({"mask": "999.999.999-99"});
        });
    </script>

@stop