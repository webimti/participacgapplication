@extends('adminlte::page')


@section('content_header')
    <h1>Setores</h1>
@stop

@section('content')
    <div class="box">
        @can('setores_create')
            <div class="box-header">
                <h3 class="box-title">
                    <a href="{{route('setores.create')}}" class="btn btn-success"><i class="fa fa-plus-square"></i> &nbsp; &nbsp;Criar Novo</a>
                </h3>

            </div>
    @endcan
    <!-- /.box-header -->
        <div class="box-body">
            <table id="datatableGrid" class="table table-bordered table-striped">
                <thead>
                <tr id="searchHeader">
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>
                        <input class="relation_rota" type="hidden" value="{{route('orgaos.json')}}">
                        <input class="relation_campo" type="hidden" value="sigla">
                    </th>
                    <th></th>
                    @can('setores_update')
                        <th></th>
                    @endcan
                </tr>
                <tr>
                    <th>ID </th>
                    <th>SIGLA</th>
                    <th>NOME</th>
                    <th>ORGÃO</th>
                    <th>ATIVO</th>
                    @can('setores_update')
                        <th>#</th>
                    @endcan
                </tr>

                </thead>
                <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    @can('setores_update')
                        <td></td>
                    @endcan
                </tr>

                </tbody>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>SIGLA</th>
                    <th>NOME</th>
                    <th>ORGÃO</th>
                    <th>ATIVO</th>
                    @can('setores_update')
                        <th>#</th>
                    @endcan
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <script>

    </script>
@stop

@section('js')

    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/Inputmask-4.x/dist/jquery.inputmask.bundle.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/participacg.js') !!}"></script>

    <script>
        $(function () {

            var linkEdit = '{{route('setores.edit',999)}}'; /***config***/

            var table = $('#datatableGrid').DataTable({
                "dom": 'lrtip',
                "serverSide": true,
                "processing": true,
                "pageLength": 25,
                "lengthMenu": [[25, 50, 100], [25, 50, 100]],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
                },
                ajax: {
                    url: '{{route('setores.json')}}',  /***config***/
                    dataSrc: 'data',
                    "dataType": "json",
                    "type": "GET",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                    /***config***/
                columns: [
                    { data: 'id'    },
                    { data: 'sigla' },
                    { data: 'name'  },
                    { data: 'orgao_id' },
                    { data: 'ativo', render:function(data){return (data==1)?'SIM':'NÃO';} },
                        @can('setores_update')
                    { data: 'id', render:function(data){return  '<a class="btn btn-small btn-primary" href="'+linkEdit.split('999').join(data)+'"><i class="fa fa-edit"></i></a>';} },
                    @endcan
                ],
                    /***config***/
                columnDefs: [
                    { targets: [ 0],     type: "number",     searchable: true,     sortable: true},
                    { targets: [ 1],     type: "string",     searchable: true,     sortable: true},
                    { targets: [ 2],     type: "string",     searchable: true,     sortable: true},
                    { targets: [ 3],     type: "relation",   searchable: true,     sortable: false},
                    { targets: [ 4],     type: "boolean",    searchable: true,     sortable: true},
                        @can('setores_update')
                    { targets: [ 5],     type: "null",       searchable: false,    sortable: false}
                    @endcan
                ],
            });

            //oculta o box search
            $(".dataTables_filter").hide();

            datatablesGeraCamposPesquisaveis(table);

            //inicializa a máscara
            $('.inputmask').inputmask();


        });
    </script>
@stop
