@extends('adminlte::page')


@section('content_header')
    <h1>Permissões/Papéis</h1>
@stop

@section('content')
    <div class="box">

    <!-- /.box-header -->
        <div class="box-body">
            {!! Form::open(['route' => 'permissions.roles.update']) !!}
            <table id="datatableGrid" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    @foreach ($roles::habilitados() as $role)
                        <th>{{ $role->name }}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach ($permissions::habilitados() as $permission)
                    <tr>
                        <td>{{ $permission->name }}</td>
                        @foreach ($roles::habilitados() as $role)
                            <th>
                                @foreach ($permissionsroles::all() as $permissionrole)
                                    @if ($permissionrole->permission_id == $permission->id &&  $permissionrole->role_id == $role->id)
                                        @php
                                            $setado="ok"
                                        @endphp
                                        @break
                                    @endif

                                @endforeach
                                    {!! Form::checkbox('perm_role['.$permission->id.']['.$role->id.']',1,($setado=="ok"),["class"=>"flat-red"]) !!}
                                    @php
                                        $setado=false
                                    @endphp
                            </th>
                        @endforeach
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    <th>#</th>
                    @foreach ($roles::habilitados() as $role)
                        <th>{{ $role->name }}</th>
                    @endforeach
                </tr>
                </tfoot>
            </table>
            <button type="submit" class="btn btn-primary">Salvar</button>
            {!! Form::close() !!}

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <script>

    </script>
@stop

@section('js')

    <script type="text/javascript" src="{!! asset('js/participacg.js') !!}"></script>

    <script>
        $(function () {

            var linkEdit = '{{route('permissions.edit',999)}}'; /***config***/


        });
    </script>
@stop
