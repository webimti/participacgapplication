<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title_prefix', config('adminlte.title_prefix', ''))
        @yield('title', config('adminlte.title', 'AdminLTE 2'))
        @yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('portal/css/style.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('portal/css/extra.css') }}"/>
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}"/>

    @yield('css')

    <script>
        /*variavel global que contém as configurações de idioma do objeto datepickrange*/
        var datepickrangerLocale = {
            "format": "YYYY-MM-DD",
            "separator": " - ",
            "applyLabel": "Aplicar",
            "cancelLabel": "Cancelar",
            "fromLabel": "De",
            "toLabel": "Até",
            "customRangeLabel": "Customizar",
            "weekLabel": "W",
            "daysOfWeek": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
            "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            "firstDay": 1
        };
    </script>

</head>
<body>
<!--header-->
<header class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
            <img class="img-responsive" title="Prefeitura de Campo Grande" src="{{ asset('portal/images/brasao-pmcg.png') }}" srcset="{{ asset('portal/images/brasao-pmcg.png') }} 1x, {{ asset('portal/images/brasao-pmcg@2x.png') }} 2x, {{ asset('portal/images/brasao-pmcg@2x.png') }} 3x, {{ asset('portal/images/brasao-pmcg@2x.png') }} 4x">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
            <h1><img src="{{ asset('portal/images/logo-scap.png') }}" class="img-responsive" srcset="{{ asset('portal/images/logo-scap.png') }} 1x, {{ asset('portal/images/logo-scap@2x.png') }} 2x, {{ asset('portal/images/logo-scap@2x.png') }} 3x, {{ asset('portal/images/logo-scap@2x.png') }} 4x" title="ParticipaCG"></h1>
        </div>
    </div>
</header>
<!--/header-->
<!--main-container-->
@yield('content')
<!--/main-container-->
<!--footer-->
<footer class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper">
                <img class="img-responsive" src="{{ asset('portal/images/brasao-v.png') }}" srcset="{{ asset('portal/images/brasao-v.png') }} 1x, {{ asset('portal/images/brasao-v@2x.png') }} 2x, {{ asset('portal/images/brasao-v@2x.png') }} 3x, {{ asset('portal/images/brasao-v@2x.png') }} 4x" title="Prefeitura Municipal de Campo Grande">
                <p>SCAP - SISTEMA DE CAPACITAÇÃO DE RECURSOS HUMANOS</p>
            </div>
        </div>
    </div>
</footer>
<!--/footer-->

{{--

Overley: Exibe uma tela sobre o conteúdo
Passar para a view um Array como parâmetro com no nome [overlay] com os possíveis campos:
    title: titulo da janela
        view: caminho da view a ser carregada
        ou
        html: texto ou html a ser exibido
    close: true ou false (se exibe botão fechar)

--}}
@isset($overlay)
<div id="scap-overlay" class="scap-overlay ">
    <div class="message">
        <h3>{{$overlay['title']}}</h3>
        <hr>
        {{--se passou texto, exibe--}}
        @isset($overlay['html'])
            <div>{!! $overlay['html'] !!}</div>
            <hr>
        @endisset
        {{--se passou view, inclui--}}
        @isset($overlay['view'])
            <div>@include($overlay['view'])</div>
            <hr>
        @endisset
        {{--se close é true, mostra botão fechar--}}
        @isset($overlay['close'])
            @if($overlay['close'])
        <button type="button" onclick="$('#scap-overlay').addClass('hide')" class="btn btn-success btn-sm">Fechar</button>
            @endif
        @endisset
    </div>
</div>
@endisset

<div id="scap-overlay-js" class="scap-overlay hide">
    <div class="message">
        <h3></h3>
        <hr>
        <div class="text-left"></div>
        <hr>
        <button type="button" onclick="$('#scap-overlay-js').addClass('hide')" class="btn btn-success btn-sm">Fechar</button>
    </div>
</div>



<script src="{{ asset('portal/js/scap-functions.min.js') }}"></script>
<script src="{{ asset('js/participacg.js') }}"></script>


@yield('js')
</body>
</html>
