function datatablesGeraCamposPesquisaveis(tabela){
    // Aplica a busca search
    tabela.columns().every( function (index) {
        //declara as variaveis
        var that, title, type, searchable;
        //cria o elemento div
        var elem_div = document.createElement("div");
        //cria o input
        var elem_inp = document.createElement("input");

        //seta as variaveis
        that = this;
        title = that.settings()[0].aoColumns[index].sTitle;
        type = that.settings()[0].aoColumns[index]._sManualType;
        searchable = that.settings()[0].aoColumns[index].bSearchable;


        //configura o elemento div
        elem_div.setAttribute('class','input-group');
        elem_div.setAttribute('style','width:100%;');
        //configura o elemento input
        elem_inp.setAttribute('placeholder','Filtrar por ' + title);
        elem_inp.setAttribute('class','form-control');
        elem_inp.setAttribute('style','width:100%;');

        //verifica se a coluna é pesquisável, para entao criar o campo
        if(searchable){
            if(type=="string"){
                elem_inp.setAttribute('type','text'); //seta o tipo do input
                //adiciona o evento de pesquisa
                $(elem_inp).on( 'keyup change', function () { if ( that.search() !== this.value ) { that.search( this.value ).draw(); } } );
                elem_div.appendChild(elem_inp);//adiciona o input ao div
                $($('#datatableGrid thead tr#searchHeader th')[index]).append(elem_div); //adiciona o div e o input ao header do datatable
            }
            else if(type=="number"){
                elem_inp.setAttribute('type','number'); //seta o tipo do input
                elem_inp.setAttribute('min','0'); //seta o atributo valor minimo
                elem_inp.setAttribute('max','99999999'); //seta o atributo valor maximo
                //adiciona o evento de pesquisa
                $(elem_inp).on( 'keyup change', function () { if ( that.search() !== this.value ) { that.search( this.value ).draw(); } } );
                elem_div.appendChild(elem_inp);//adiciona o input ao div
                $($('#datatableGrid thead tr#searchHeader th')[index]).append(elem_div); //adiciona o div e o input ao header do datatable
            }
            else if(type=="boolean"){
                elem_inp = document.createElement("select");
                elem_inp.setAttribute('class','form-control');
                elem_inp.setAttribute('id','select_boolean');
                //cria a opcao todos
                var elem_opt = document.createElement("option");
                elem_opt.setAttribute('value','todos');
                elem_opt.innerText = 'Todos';
                elem_inp.appendChild(elem_opt);
                //cria a opcao ativo
                elem_opt = document.createElement("option");
                elem_opt.setAttribute('value','ativo');
                elem_opt.innerText = 'Sim';
                elem_inp.appendChild(elem_opt);
                //cria a opcao ativo
                elem_opt = document.createElement("option");
                elem_opt.setAttribute('value','inativo');
                elem_opt.innerText = 'Não';
                elem_inp.appendChild(elem_opt);
                //adiciona o evento de pesquisa
                $(elem_inp).on( 'change', function () { if ( that.search() !== this.value ) { that.search( this.value ).draw(); } } );
                elem_div.appendChild(elem_inp);//adiciona o input ao div
                $($('#datatableGrid thead tr#searchHeader th')[index]).append(elem_div); //adiciona o div e o input ao header do datatable

                $('#select_boolean').select2();


            }
            else if (type=="datetime"){
                /*
                elem_inp.setAttribute('type','text'); //seta o tipo do input
                elem_inp.setAttribute('id','date_range'); //seta o tipo do input
                //adiciona o evento de pesquisa
                $(elem_inp).on( 'keyup change', function () { if ( that.search() !== this.value ) { that.search( this.value ).draw(); } } );
                elem_div.appendChild(elem_inp);//adiciona o input ao div
                $($('#datatableGrid thead tr#searchHeader th')[index]).append(elem_div); //adiciona o div e o input ao header do datatable
                //inicializa a máscara
                $('#date_range').daterangepicker({
                    "showDropdowns": true,
                    "format": 'YYYY-MM-DD',
                    "startDate": "1919-01-01",
                    "locale": {
                        "format": "YYYY-MM-DD",
                        "separator": " - ",
                        "applyLabel": "Aplicar",
                        "cancelLabel": "Cancelar",
                        "fromLabel": "De",
                        "toLabel": "Até",
                        "customRangeLabel": "Customizar",
                        "weekLabel": "W",
                        "daysOfWeek": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                        "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                        "firstDay": 1
                    }
                });
                */
            }
            else if (type=="relation"){
                /*Limite de registros por consulta*/
                var limiteConsultaRelation = 10;
                /*pega a rota de onde deve buscar os itens (o mesmo do datatable)*/
                var rotaSelect2Ajax = $($('#datatableGrid thead tr#searchHeader th')[index]).find('input.relation_rota').val();
                /*pega o nome do campo a ser exibido*/
                var rotaSelect2CampoFiltro = $($('#datatableGrid thead tr#searchHeader th')[index]).find('input.relation_campo').val();
                elem_inp = document.createElement("select");
                elem_inp.setAttribute('class','form-control');
                elem_inp.setAttribute('id','select_relation');
                //cria a opcao todos
                var elem_opt = document.createElement("option");
                elem_opt.setAttribute('value','todos');
                elem_opt.innerText = 'Todos';
                elem_inp.appendChild(elem_opt);

                //adiciona o evento de pesquisa
                $(elem_inp).on( 'change', function () { if ( that.search() !== this.value ) { that.search( this.value ).draw(); } } );
                elem_div.appendChild(elem_inp);//adiciona o input ao div
                $($('#datatableGrid thead tr#searchHeader th')[index]).append(elem_div); //adiciona o div e o input ao header do datatable
                $(elem_inp).select2({
                    ajax: {
                        url: rotaSelect2Ajax,
                        dataType: 'json',
                        cache: true,
                        data: function (params) {
                            params.page = (params.page) ? params.page:0;
                            var query = {
                                columns: [
                                    {
                                        data: 'id',
                                        searchable : false,
                                        orderable : false,
                                        'search': {
                                            value: ''
                                        }
                                    },{
                                        data: rotaSelect2CampoFiltro,
                                        searchable : true,
                                        orderable : false,
                                        'search': {
                                            value: params.term?params.term:''
                                        }
                                    }
                                ],
                                order: [{
                                    column: 1,
                                    dir : 'asc'
                                }],
                                draw : 1,
                                start :  (params.page*limiteConsultaRelation),
                                length : limiteConsultaRelation,
                                '_': Math.random()
                            };
                            return query;
                        },
                        processResults: function (data,params) {
                            var arr = Object.values(data.data);
                            //console.log(arr);
                            //arr.push({id: "" , sigla: "TODOS"});
                            return {
                                results: dat = JSON.parse(JSON.stringify(arr).split(rotaSelect2CampoFiltro).join('text')),
                                pagination: {
                                    more: (params.page*limiteConsultaRelation+data.recordsTotal)  < data.recordsFiltered
                                }
                            };
                        }
                    }
                });
            }

        }

        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
}/*function datatablesGeraCamposPesquisaveis*/

/*valida CPF*/
function validaCPF(cpf)
{
    console.log(cpf);
    /*remove a mascara, se houver*/
    cpf = cpf.split('.').join('').split('-').join('');
    console.log(cpf);

    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    if (cpf.length < 11)
        return false;
    for (i = 0; i < cpf.length - 1; i++)
        if (cpf.charAt(i) != cpf.charAt(i + 1))
        {
            digitos_iguais = 0;
            break;
        }
    if (!digitos_iguais)
    {
        numeros = cpf.substring(0,9);
        digitos = cpf.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--)
            soma += numeros.charAt(10 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        numeros = cpf.substring(0,10);
        soma = 0;
        for (i = 11; i > 1; i--)
            soma += numeros.charAt(11 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    }
    else
        return false;
}

/*exibe overlay com mensagem*/
function alerta(titulo,mensagens){
    var mensagem, conteudo="<ul>";
    while(mensagem=mensagens.shift()){
        conteudo += "<li>"+mensagem+"</li>"
    }
    conteudo+="</ul>";

    $('#scap-overlay-js').removeClass('hide');
    $('#scap-overlay-js div.message h3').text(titulo);
    $('#scap-overlay-js div.message div').html(conteudo);
}