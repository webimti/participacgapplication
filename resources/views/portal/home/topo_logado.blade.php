<div class="container inner-nav">
    <div class="row">
        <div class="col-md-12">
            <div class="blue-bar"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper">
                <p class="hello">Olá, <strong>{{ Auth::user()->name }}</strong>!</p>
                <a href="#" title="Abrir menu" class="toggle-menu visible-xs">MENU <i class="glyphicon glyphicon-menu-hamburger"></i></a>
                <nav>
                    <ul>
                        <li><a href="{{route('portal.home')}}" title=""><i class="glyphicon glyphicon-home"></i> HOME</a></li>
                        <li><a href="{{route('portal.painel')}}" title=""><i class="glyphicon glyphicon-home"></i> PAINEL</a></li>
                        <li><a href="#" title=""><i class="glyphicon glyphicon-calendar"></i> CRONOGRAMA</a></li>
                        <li><a href="{{route('portal.painel.dados')}}" title=""><i class="glyphicon glyphicon-book"></i> MEUS DADOS</a></li>
                        <li><a href="{{route('portal.logout')}}" title=""><i class="glyphicon glyphicon-log-out"></i> SAIR</a></li>
                    </ul>
                </nav>
            </div>

            <div class="area-title">
                <h2>
                    @empty($TITLE)
                        NOVO <strong>CADASTRO</strong>
                    @endempty

                    @isset($TITLE)
                        @if(is_array($TITLE))
                            {{array_shift($TITLE)}} <strong>{{array_pop($TITLE)}}</strong>
                        @else
                        {!! $TITLE !!}
                        @endif
                    @endisset
                </h2>
            </div>
        </div>
    </div>
</div>