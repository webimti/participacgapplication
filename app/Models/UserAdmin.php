<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Role_User;

class UserAdmin extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','email','nascimento','cpf','ativo','password'];

    protected $table = 'users';

    public function roles()
    {
        return Role_User::roles($this);
    }
}
