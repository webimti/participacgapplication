<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDneGuBairrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //cria a tabela bairros
        Schema::create('dne_gu_bairros', function (Blueprint $table) {

            $table->integer('chave_bai_dne')->unsigned();
            $table->integer('chave_loc_dne')->unsigned();
            $table->string('sigla_uf_bai',2)->nullable();
            $table->string('nome_ofi_bai',72)->nullable();
            $table->string('abre_bai_rec_ect',36)->nullable();

            $table->primary('chave_bai_dne');

            $table->foreign('chave_loc_dne')
                ->references('chave_loc_dne')
                ->on('dne_gu_localidades')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dne_gu_bairros');
    }
}
