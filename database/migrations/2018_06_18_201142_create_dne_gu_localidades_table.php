<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDneGuLocalidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dne_gu_localidades', function (Blueprint $table) {
            $table->integer('chave_loc_dne')->unsigned();
            $table->string('sigla_uf',2)->nullable();
            $table->string('nome_ofi_localidade',72)->nullable();
            $table->string('cep_localidade',8)->nullable();
            $table->string('abrev_loc_rec_ect',36)->nullable();
            $table->string('tipo_localidade',1)->nullable();
            $table->string('situacao_localidade',1)->nullable();
            $table->integer('chave_sub_loc_dne')->unsigned()->nullable();
            $table->string('sigla_dr_ect_loc',3)->nullable();
            $table->string('codigo_mun_ibge',7)->nullable();

            $table->primary('chave_loc_dne');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dne_gu_localidades');
    }
}
