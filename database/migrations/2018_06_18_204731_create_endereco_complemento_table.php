<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnderecoComplementoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('endereco_complemento', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            //chave do logradouro
            $table->integer('chave_logradouro_dne');
            //chave estrangeira de proprietario
            $table->integer('chave_proprietario');
            //tipo do proprietário (usuario, evento, orgao, etc)
            $table->tinyInteger('tipo_proprietario');

            //chave única para um logradouro ligado a um proprietario do mesmo tipo
            //$table->unique(['chave_logradouro_dne','chave_proprietario','tipo_proprietario']);


            //chave com logradouros
            $table->foreign('chave_logradouro_dne')
                ->references('chave_logradouro_dne')
                ->on('dne_gu_logradouros')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('endereco_complemento');
    }
}
