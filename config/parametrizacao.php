<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Frontend
    |--------------------------------------------------------------------------
    */

    /*Idade mínima para cadastro - em meses*/
    'frontend_cadastro_idade_minima_meses' => '6',

    /*Idade mínima para não necessitar responsável - em anos*/
    'frontend_cadastro_idade_minima_responsavel' => '18',

    /*Telefone da central de atendimento*/
    'fone_central_atendimento' => '3314-3333',


];
