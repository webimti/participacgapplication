@extends('layouts.portal')

@section('content')

    @php $TITLE = ['PÁGINA ','INICIAL'] @endphp
<div class="container main-container">


    <!--welcoming-->
    <div class="container home-banner">
        <div class="row">
            <div class="col-md-12">
                <div class="green-bar">

                </div>
            </div>
        </div>
        <div class="row">
            @if (Auth::check())
                @include('portal.home.topo_logado')
            @else
                <div class="col-md-12">
                    <img src="{{ asset('portal/images/scap-home-welcoming.jpg') }}" class="img-responsive" title="">
                    <!--login-form-->
                    <div class="home-login">
                        <h3><i class="glyphicon glyphicon-user"></i>LOGIN <strong>DO ALUNO</strong></h3>
                        {!! Form::open(['route' => 'portal.login']) !!}
                            <div class="field-wrapper">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="CPF" id="cpf" name="cpf" required>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="SENHA" id="senha" name="senha" required>
                                </div>
                            </div>
                            <div class="submit-wrapper">
                                <div class="form-group">
                                    <a href="{{route('portal.senha.etapa1',1)}}" title="Recuperar senha"><i class="glyphicon glyphicon-lock"></i> Esqueci minha senha</a><input type="submit" class="btn btn-primary" value="ENTRAR">
                                </div>

                            </div>
                        {!! Form::close() !!}
                    </div>
                    <!--/login-form-->
                </div>
            @endif
        </div>
        <div class="row welcoming-bottom">
            <div class="col-md-12">
                <div class="wrapper">
                    <!--item-->
                    <div>
                        <h3><i class="glyphicon glyphicon-education"></i> O QUE É</h3>
                        <div class="info">
                            <p>A Prefeitura de Campo Grande oferece - durante todo o ano - cursos gratuitos de capacitação profissional através da Escola de Governo.</p>
                        </div>
                    </div>
                    <!--/item-->
                    <!--item-->
                    <div>
                        <h3><i class="glyphicon glyphicon-user"></i> A QUEM SE DESTINA</h3>
                        <div class="info">
                            <p>Aos funcionários da prefeitura e também ao cidadão de Campo Grande, basta de inscrever e ficar atento ao cronograma de cursos.</p>
                        </div>
                    </div>
                    <!--/item-->
                    <!--item-->
                    <div>
                        <h3><i class="glyphicon glyphicon-ok"></i> COMO SE INSCREVER</h3>
                        <div class="info">
                            <p>Para se inscrever basta preencher o formulário de inscrição, fornecendo seus dados e selecionar um dos cursos disponíveis.</p>
                            @unless (Auth::check())
                            <a class="btn btn-large signup" href="{{route('portal.cadastro.etapa1',1)}}" title="Fazer Inscrição">FAZER <strong>INSCRIÇÃO</strong></a>
                            @endunless
                        </div>
                    </div>
                    <!--/item-->
                </div>
            </div>
        </div>
    </div>
    <!--/welcoming-->
    <!--calendar-->
    <div class="container calendar">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title"><strong>CRONOGRAMA</strong> DE CURSOS</h3>
            </div>
        </div>
        <!--list-->
        <div class="row calendar-list">
            <!--calendar-item-->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a class="calendar-item" href="#" title="Curso">
                    <div class="date">
                        <span class="month">SETEMBRO</span>
                        <strong class="day">26</strong>
                    </div>
                    <div class="info">
                        <h3>DIREITOS HUMANOS NO CONTEXTO DAS POLÍTICAS PÚBLICAS</h3>
                        <small>PÚBLICO ALVO</small>
                        <p>Servidores públicos, lidernaças comunitárias, integrantes de movimentos sociais e universitários.</p>
                    </div>
                </a>
            </div>
            <!--/calendar-item-->
            <!--calendar-item-->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a class="calendar-item" href="#" title="Curso">
                    <div class="date">
                        <span class="month">SETEMBRO</span>
                        <strong class="day">26</strong>
                    </div>
                    <div class="info">
                        <h3>DIREITOS HUMANOS NO CONTEXTO DAS POLÍTICAS PÚBLICAS</h3>
                        <small>PÚBLICO ALVO</small>
                        <p>Servidores públicos, lidernaças comunitárias, integrantes de movimentos sociais e universitários. </p>
                    </div>
                </a>
            </div>
            <!--/calendar-item-->
            <!--calendar-item-->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a class="calendar-item" href="#" title="Curso">
                    <div class="date">
                        <span class="month">SETEMBRO</span>
                        <strong class="day">26</strong>
                    </div>
                    <div class="info">
                        <h3>DIREITOS HUMANOS NO CONTEXTO DAS POLÍTICAS PÚBLICAS</h3>
                        <small>PÚBLICO ALVO</small>
                        <p>Servidores públicos, lidernaças comunitárias, integrantes de movimentos sociais e universitários.</p>
                    </div>
                </a>
            </div>
            <!--/calendar-item-->
            <!--calendar-item-->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a class="calendar-item" href="#" title="Curso">
                    <div class="date">
                        <span class="month">SETEMBRO</span>
                        <strong class="day">26</strong>
                    </div>
                    <div class="info">
                        <h3>DIREITOS HUMANOS NO CONTEXTO DAS POLÍTICAS PÚBLICAS</h3>
                        <small>PÚBLICO ALVO</small>
                        <p>Servidores públicos, lidernaças comunitárias, integrantes de movimentos sociais e universitários.</p>
                    </div>
                </a>
            </div>
            <!--/calendar-item-->
            <!--calendar-item-->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a class="calendar-item" href="#" title="Curso">
                    <div class="date">
                        <span class="month">SETEMBRO</span>
                        <strong class="day">26</strong>
                    </div>
                    <div class="info">
                        <h3>DIREITOS HUMANOS NO CONTEXTO DAS POLÍTICAS PÚBLICAS</h3>
                        <small>PÚBLICO ALVO</small>
                        <p>Servidores públicos, lidernaças comunitárias, integrantes de movimentos sociais e universitários.</p>
                    </div>
                </a>
            </div>
            <!--/calendar-item-->

        </div>
        <!--/list-->
        <!--separator-->
        <div class="row separator">
            <div class="col-md-12">
                <div class="wrapper"></div>
            </div>
        </div>
        <!--/separator-->
    </div>
    <!--calendar-->
</div>
@stop



@section('js')
    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/Inputmask-4.x/dist/jquery.inputmask.bundle.js') !!}"></script>
    <script>
        function validaFormCadastro(form){
            var erros=[];


            if(form.cpf.value!="" && !validaCPF(form.cpf.value)) erros.push("O CPF do Beneficiário é inválido.");

            if(form.senha.value.length<6) erros.push("A senha deve conter no mínimo 6 caracteres.");

            if(erros.length>0){
                alerta('Existem erros',erros);
                return false;
            }
            return true;
        }



        $(function () {
            $('#cpf').inputmask({"mask": "999.999.999-99"});
        });
    </script>

@stop