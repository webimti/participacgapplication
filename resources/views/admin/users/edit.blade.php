@extends('adminlte::page')


@section('content_header')
    <h1>Usuários</h1>
@stop


<?php
    $rotaAtual = Illuminate\Support\Facades\Route::currentRouteName();
    $request = new \Illuminate\Http\Request();
    $aba = (isset($aba))?$aba:'dados';

?>


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">

        @if ($rotaAtual=="users.create")
                @include('admin.users.edit_formulario')
        @else

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class=" {{($rotaAtual=='users.edit')?'active':''}}">        <a href="{{route('users.edit',$model->id)}}"  >Dados</a></li>
                            <li class=" {{($rotaAtual=='users.roles.index')?'active':''}}"> <a href="{{route('users.roles.index',$model->id)}}"  >Papéis</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="">
                                @if ($rotaAtual=="users.edit")
                                    @include('admin.users.edit_formulario')
                                @else
                                    @include('admin.role_user.form')
                                @endif
                            </div>

                        </div>
                    </div>
        @endif
        </div>
    </div>
</div>


    <script>

    </script>
@stop

@section('css')
    <link rel="stylesheet" href="{!! asset('vendor/adminlte/plugins/iCheck/skins/all.css') !!}">
    <link rel="stylesheet" href="{!! asset('vendor/adminlte/plugins/daterangepicker-master/daterangepicker.css') !!}">
@stop
@section('js')

    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/iCheck/icheck.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/daterangepicker-master/moment.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/daterangepicker-master/daterangepicker.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/inputmask-4.x/dist/jquery.inputmask.bundle.js') !!}"></script>

    <script>
        $(function () {
            //Flat red color scheme for iCheck
            $('input[type="checkbox"]').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            });

            $('.inputmask').inputmask();


            /*campos Papel e Orgaos (cotrole de required e visibilidade)*/
            $('#orgaos_select').select2();
            @if(!$errors->has('orgao'))
            $('#orgaos_select_box').hide();
            $('#orgaos_select').removeAttr('required');
            @endif
            $('#roles_select').select2().on('select2:select', function (e) {
                if(e.params.data.text.indexOf('*') >= 0){
                    $('#orgaos_select_box').show();
                    $('#orgaos_select').attr('required', 'required');
                }else{
                    $('#orgaos_select_box').hide();
                    $('#orgaos_select').removeAttr('required');
                }
            });
            /*campos Papel e Orgaos*/



            $('#date_field').daterangepicker({ "showDropdowns": true, "format": 'DD/MM/YYYY',
                "singleDatePicker": true,
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aplicar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "De",
                    "toLabel": "Até",
                    "customRangeLabel": "Customizar",
                    "weekLabel": "W",
                    "daysOfWeek": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                    "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                    "firstDay": 1
                }
            });
        });
    </script>
@stop


