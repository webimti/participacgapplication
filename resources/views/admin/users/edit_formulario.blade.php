<div class="box-header with-border">
    <h3 class="box-title">Preencha os campos  </h3>
</div>
{!! Form::model($model, $routeModel) !!}
@if (isset($model->ativo))
    <input type="hidden" name="_method" value="put" />
@endif
<input type="hidden" id="aba" name="aba" value="dados" />
<div class="box-body row">
    <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 {{ $errors->has('name') ? 'has-error' : '' }}">
        <label for="">Nome <i class="text-danger text-sm fa fa-asterisk"></i></label>
        {!! Form::text('name',null,['class' => 'form-control','required'=>true]) !!}
        @if ($errors->has('name'))
            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>
    <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 {{ $errors->has('email') ? 'has-error' : '' }}">
        <label for="">Email
            @if($rotaAtual=="users.create")
                <i class="text-danger text-sm fa fa-asterisk"></i>
            @endif
        </label>
        {!! Form::text('email',null,['class' => 'form-control','required'=>($rotaAtual=="users.create")]) !!}
        @if ($errors->has('email'))
            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
        @endif
    </div>
    <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 {{ $errors->has('cpf') ? 'has-error' : '' }}">
        <label for="">CPF
            @if($rotaAtual=="users.create")
                <i class="text-danger text-sm fa fa-asterisk"></i>
            @endif
        </label>
        {!! Form::text('cpf',null,['class' => 'form-control inputmask','required'=>($rotaAtual=="users.create"), 'data-inputmask'=>"'alias': '999.999.999-99'", 'data-mask'=>""]) !!}
        @if ($errors->has('cpf'))
            <span class="help-block"><strong>{{ $errors->first('cpf') }}</strong></span>
        @endif
    </div>
    <div class="form-group  col-xs-12 col-sm-12 col-md-6 col-lg-6 {{ $errors->has('nascimento') ? 'has-error' : '' }}">
        <label>Nascimento  <i class="text-danger text-sm fa fa-asterisk"></i></label>
        @php
            if(isset($model->nascimento)){
                list($year,$month,$day) = explode("-",$model->nascimento);
                $campoData= "{$day}/{$month}/{$year}";
            }else{
                $campoData = date("d/m/Y");
            }
        @endphp
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            {!! Form::text('nascimento',$campoData,['class' => 'form-control','required'=>true,'id'=>'date_field']) !!}
            @if ($errors->has('nascimento'))
                <span class="help-block"><strong>{{ $errors->first('nascimento') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <label>
            @if (isset($model->ativo))
                {!! Form::checkbox('ativo',1,($model->ativo),["class"=>"flat-red"]) !!}
            @else
                {!! Form::checkbox('ativo',1,true,["class"=>"flat-red"]) !!}
            @endif
            Registro Ativo?
        </label>
    </div>
</div>
<div class="box-footer">
    <button type="submit" class="btn btn-primary">Salvar</button>
    <button type="button" class="btn btn-warning pull-right" onclick="window.location='{{route('users.index')}}';">Cancelar</button>
</div>
{!! Form::close() !!}
{{--formulario--}}