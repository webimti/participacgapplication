<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserAdmin;
use App\Models\Role;
use App\Models\Orgao;
use App\Models\Role_User;

class RoleUserController extends Controller
{

    public function index($user_id)
    {
        $this->authorize('adm');

        $user = UserAdmin::find($user_id);
        $rolesQuery = Role::habilitadosAll()->toArray();
        $roles=[];
        for($i=0; $i<sizeof($rolesQuery);$i++){
            if($rolesQuery[$i]['regional'])
                $roles[$rolesQuery[$i]['id']] = $rolesQuery[$i]['name']." (* Requer órgão)";
            else
                $roles[$rolesQuery[$i]['id']] = $rolesQuery[$i]['name'];
        }
        return view('admin.users.edit',array('model'=>$user,'routeModel'=>['route' => ['users.roles.store', $user_id]],'role_user'=>$user->roles(),'roles'=>$roles,'orgaos'=>Orgao::habilitadosQuery()->pluck('name','id')->toArray()));
    }



    public function store(Request $request,$user_id)
    {
        $this->authorize('adm');


        //recupera o Role enviado pelo form
        $role = Role::find($request->role);

        //campos a serem inseridos no relacionamento
        $fieldsToStore = [];

        $fieldsToStore['role_id'] = $request->role;
        $fieldsToStore['user_id'] = $user_id;

        //verifica se o Role exige Orgao, e se Orgao NAO foi enviado pelo Form
        if($role->regional && !$request->filled('orgao')){
            //gera erro de orgao
            return redirect()->route('users.roles.index',$user_id) ->withErrors(['orgao'=>'Este Papel é Regional e exige um Órgão'])->withInput();
        }else if($role->regional && $request->filled('orgao')){
            $fieldsToStore['orgao_id'] = $request->orgao;
        }

        //faz a ação CRUD
        $objModelInserted = Role_User::updateOrCreate($fieldsToStore,$fieldsToStore);
        $wasCreated = $objModelInserted->wasRecentlyCreated;

        if($wasCreated)
            return redirect()->route('users.roles.index',$user_id) ->with('msgSuccess','Papel atrbuído com sucesso.');
        else
            return redirect()->route('users.roles.index',$user_id) ->withErrors(['Este Papel já está atribuído']);

    }




    public function destroy(Request $request,$user_id)
    {
        $this->authorize('adm');

        $request_fields = $request->all();


        Role_User::find($request_fields['id'])->delete();


        return redirect()->route('users.roles.index',$user_id) ->with('msgSuccess','Papel ['.$request_fields['name'].'] revogado com sucesso.');

    }
}
