<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Orgao;
use App\Models\Setor;
use Illuminate\Support\Facades\Storage;

class SetorController extends Controller
{
    public function index()
    {
        $this->authorize('setores_view');
        return view('admin.setor.index');
    }


    public function json(Request $request)
    {
        $this->authorize('setores_view');
        $datatablejson = new Setor();

        ob_start();
        print_r($request->all());
        $obob = ob_get_clean();
        Storage::disk('local')->put('datatables-setores.txt', $obob);


        //['campo_id'=>['model'=>Model::class, 'tabela'=>'nome_tabela', 'campo_view'=>'campo_visivel', 'campo_json'=>'nome_a_enviar_ao_json']]

        return $datatablejson->dataTable($request,['orgao_id'=>['tabela'=>'orgaos', 'campo_view'=>'sigla', 'campo_json'=>'orgao_id']]);
    }


    public function create()
    {
        $this->authorize('orgaos_create');
        return view('admin.orgao.edit',array('model'=>Orgao::class,'routeModel'=>['route' => ['orgaos.store']],'validator'=>null));
    }


    public function store(Request $request)
    {
        //verifica permissao
        $this->authorize('orgaos_create');
        //valida os campos enviados
        $validator = Validator::make($request->all(), [
            'sigla' => 'required|min:3|max:50',
            'name' => 'required|min:3|max:255',
        ]);
        //se falhou validação, envia novamente para o formulário
        if ($validator->fails()) {
            return redirect()->route('orgaos.create') ->withErrors($validator)->withInput();

        }
        //instancia o Model para executar a query CRUD
        $objModel = new Orgao();
        //remove os campos extras enviados pelo formulario e deixa apenas os campos que coincidem com as colunas da tabela
        $fieldsToUpdate = array_intersect_key($request->all(),array_flip($objModel->getConnection()->getSchemaBuilder()->getColumnListing($objModel->getTable())) );
        //seta o status
        $fieldsToUpdate['ativo'] = isset($fieldsToUpdate['ativo']);
        //faz a ação CRUD
        $objModelInserted = $objModel->updateOrCreate(array('id'=>0),$fieldsToUpdate);
        //envia para a listagem com mensagem de sucesso
        return redirect()->route('orgaos.index') ->with('msgSuccess','Alterado com sucesso. ID: '.$objModelInserted->id.'. ');
    }


    public function edit($id)
    {
        //verifica permissao
        $this->authorize('orgaos_update');
        return view('admin.orgao.edit',array('model'=>Orgao::find($id),'routeModel'=>['route' => ['orgaos.update', $id]],'validator'=>null));
    }


    public function update(Request $request, $id)
    {
        //verifica permissao
        $this->authorize('orgaos_update');
        //valida os campos enviados
        $validator = Validator::make($request->all(), [
            'sigla' => 'required|min:3|max:50',
            'name' => 'required|min:3|max:255',
        ]);
        //se falhou validação, envia novamente para o formulário
        if ($validator->fails()) {
            return redirect()->route('orgaos.edit',$id) ->withErrors($validator)->withInput();
        }
        //instancia o Model para executar a query CRUD
        $objModel = new Orgao();
        //remove os campos extras enviados pelo formulario e deixa apenas os campos que coincidem com as colunas da tabela
        $fieldsToUpdate = array_intersect_key($request->all(),array_flip($objModel->getConnection()->getSchemaBuilder()->getColumnListing($objModel->getTable())) );
        //seta o status
        $fieldsToUpdate['ativo'] = isset($fieldsToUpdate['ativo']);
        //faz a ação CRUD
        Orgao::updateOrCreate(array('id'=>$id),$fieldsToUpdate);
        //envia para a listagem com mensagem de sucesso
        return redirect()->route('orgaos.index') ->with('msgSuccess','Alterado com sucesso. ID: '.$id.'. ');
    }
}
