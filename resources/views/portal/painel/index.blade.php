@extends('layouts.portal')

@section('content')




    <div class="container main-container">


        <!--welcoming-->
        <div class="container home-banner">
            <div class="row">
                <div class="col-md-12">
                    <div class="green-bar">

                    </div>
                </div>
            </div>
            <div class="row">
                @if (Auth::check())
                    @include('portal.home.topo_logado')
                @else
                    <div class="col-md-12">
                        <img src="{{ asset('portal/images/scap-home-welcoming.jpg') }}" class="img-responsive" title="">
                        <!--login-form-->
                        <div class="home-login">
                            <h3><i class="glyphicon glyphicon-user"></i>LOGIN <strong>DO ALUNO</strong></h3>
                            {!! Form::open(['route' => 'portal.login']) !!}
                            <div class="field-wrapper">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="CPF" id="cpf" name="cpf" required>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="SENHA" id="senha" name="senha" required>
                                </div>
                            </div>
                            <div class="submit-wrapper">
                                <div class="form-group">
                                    <a href="{{route('portal.senha.etapa1',1)}}" title="Recuperar senha"><i class="glyphicon glyphicon-lock"></i> Esqueci minha senha</a><input type="submit" class="btn btn-primary" value="ENTRAR">
                                </div>

                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!--/login-form-->
                    </div>
                @endif
            </div>
            <div class="row welcoming-bottom">
                <div class="col-md-12">

                </div>
            </div>
        </div>


        {{--LISTA PARTICIPANDO--}}
        <div class="container calendar">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title">O QUE ESTOU <strong>PARTICIPANDO</strong> </h3>
                </div>
            </div>
            <div class="row calendar-list">
                //LISTA DE ACOES
            </div>
            <div class="row separator">
                <div class="col-md-12">
                    <div class="wrapper"></div>
                </div>
            </div>
        </div>
        {{--/LISTA PARTICIPANDO--}}

        {{--LISTA PARTICIPEI--}}
        <div class="container calendar">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title">O QUE JÁ <strong>PARTICIPEI</strong> </h3>
                </div>
            </div>
            <div class="row calendar-list">
                //LISTA DE ACOES
            </div>
            <div class="row separator">
                <div class="col-md-12">
                    <div class="wrapper"></div>
                </div>
            </div>
        </div>
        {{--/LISTA PARTICIPEI--}}

        {{--LISTA ESPERA--}}
        <div class="container calendar">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title">MINHA LISTA DE <strong>ESPERA</strong> </h3>
                </div>
            </div>
            <div class="row calendar-list">
                //LISTA DE ACOES
            </div>
            <div class="row separator">
                <div class="col-md-12">
                    <div class="wrapper"></div>
                </div>
            </div>
        </div>
        {{--/LISTA ESPERA--}}



    </div>
@stop



@section('js')
    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/Inputmask-4.x/dist/jquery.inputmask.bundle.js') !!}"></script>
    <script>
        function validaFormCadastro(form){
            var erros=[];


            if(form.cpf.value!="" && !validaCPF(form.cpf.value)) erros.push("O CPF do Beneficiário é inválido.");

            if(form.senha.value.length<6) erros.push("A senha deve conter no mínimo 6 caracteres.");

            if(erros.length>0){
                alerta('Existem erros',erros);
                return false;
            }
            return true;
        }



        $(function () {
            $('#cpf').inputmask({"mask": "999.999.999-99"});
        });
    </script>

@stop