<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Role;

class Permission extends BaseModel
{
    protected $fillable = ['name','label','ativo'];

    /**
     *
     */
    public function roles(){
        //dd($this->belongsToMany(Role::class)->toSql());exit;
        return $this->belongsToMany(Role::class);
    }

    /**
     * Retorna todas as regras válidas (ativo=true) e que não seja adm (pois tem privilegio especial)
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function habilitados(){
        return Permission::query()->where('ativo',1)->orderBy('name','asc')->get();
    }
}
