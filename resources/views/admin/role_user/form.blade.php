
<div class="row">
    <div class="col-md-6">
        {{--formulario role--}}
        <div class="box-header with-border">
            <h3 class="box-title">Atribuir papéis a: <strong> {{$model->name}} #{{$model->id}}</strong></h3>
        </div>

        {!! Form::model($model, $routeModel) !!}
        <div class="box-body row">
            <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 {{ $errors->has('role') ? 'has-error' : '' }}">
                <label>Papel</label>
                {!! Form::select('role',$roles,null,['id'=>'roles_select','placeholder' => 'Escolha um Papel','style' => 'width: 100%;','class' => 'form-control select2 select2-hidden-accessible','required'=>true,'tabindex'=>'-1','aria-hidden'=>'true']) !!}
            </div>

            <div id="orgaos_select_box" class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 {{ $errors->has('orgao') ? 'has-error' : '' }}">
                <label>Órgão</label>
                {!! Form::select('orgao',$orgaos,null,['id'=>'orgaos_select','placeholder' => 'Escolha um Órgão','style' => 'width: 100%;','class' => 'form-control select2 select2-hidden-accessible','tabindex'=>'-1','aria-hidden'=>'true','required'=>true]) !!}
                @if ($errors->has('orgao'))
                    <span class="help-block"><strong>{{ $errors->first('orgao') }}</strong></span>
                @endif
            </div>
        </div>

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Conceder Permissão</button>
        </div>
        {!! Form::close() !!}
        {{--/formulario role--}}
    </div>
    <div class="col-md-6">
        {{--lista de roles--}}
        <div class="box-header with-border">
            <h3 class="box-title">Papéis atribuídos</h3>
        </div>
        <div class="box-body">
            @foreach($role_user as $single_role)

                <div class="dropdown pull-left" style="margin-right: 10px;">
                    {!! Form::open(['route' => ['users.roles.destroy', $model->id],'id'=>'form_delete_role_user_'.$single_role['id']]) !!}
                    <input type="hidden" name="id" value="{{$single_role['id']}}">
                    <input type="hidden" name="name" value="{{$single_role['name']}} {{$single_role['orgao_id']?"({$single_role['sigla']})":''}}">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        {{$single_role['name']}} {{$single_role['orgao_id']?"({$single_role['sigla']})":''}}
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="#" onclick="(confirm('Deseja realmente revogar este Papel?'))?$('#form_delete_role_user_{{$single_role['id']}}').submit(): false;">Excluir</a></li>
                    </ul>
                    {!! Form::close() !!}
                </div>


            @endforeach

        </div>
        {{--/lista de roles--}}
    </div>
</div>




