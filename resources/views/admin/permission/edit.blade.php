@extends('adminlte::page')


@section('content_header')
    <h1>Permissões</h1>
@stop


@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Preencha os campos</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::model($model, $routeModel) !!}
                @if (isset($model->ativo))
                    <input type="hidden" name="_method" value="put" />
                @endif

                <div class="box-body">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="">Nome <i class="text-danger text-sm fa fa-asterisk"></i></label>
                        {!! Form::text('name',null,['class' => 'form-control','required'=>true]) !!}
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('label') ? 'has-error' : '' }}">
                        <label for="">Descrição <i class="text-danger text-sm fa fa-asterisk"></i></label>
                        {!! Form::text('label',null,['class' => 'form-control','required'=>true]) !!}
                        @if ($errors->has('label'))
                            <span class="help-block"><strong>{{ $errors->first('label') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>
                            @if (isset($model->ativo))
                            {!! Form::checkbox('ativo',1,($model->ativo),["class"=>"flat-red"]) !!}
                            @else
                                {!! Form::checkbox('ativo',1,true,["class"=>"flat-red"]) !!}
                            @endif
                                Registro Ativo?
                        </label>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                    <button type="button" class="btn btn-warning pull-right" onclick="window.location='{{route('permissions.index')}}';">Cancelar</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


    <script>

    </script>
@stop

@section('css')
    <link rel="stylesheet" href="{!! asset('vendor/adminlte/plugins/iCheck/skins/all.css') !!}">
@stop
@section('js')

    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/iCheck/icheck.js') !!}"></script>

    <script>
        $(function () {
            //Flat red color scheme for iCheck
            $('input[type="checkbox"]').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })
        });
    </script>
@stop


