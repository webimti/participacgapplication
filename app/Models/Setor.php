<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setor extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sigla','name','ativo','orgao_id'];

    protected $table = 'setores';


    /**
     * Retorna todas orgaos habilitados
     */
    public static function habilitadosQuery(){
        return Setor::query()->where('ativo',1)->orderBy('sigla');
    }

    /**
     * Retorna todas orgaos habilitados
     */
    public static function habilitados(){
        return Setor::habilitadosQuery()->get();
    }
}
