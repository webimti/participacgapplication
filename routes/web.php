<?php


/**
 * Rotas protegidas por login e senha no painel admin
 */

Route::group(
    [
        'middleware'    =>  ['auth'],   /*requer autenticacao para todos os controles*/
        'namespace'     =>  'Admin',    /*todos controller estão dentro da pasta (namespace) Admin*/
        'prefix'        =>  'admin'     /*Atribui o prefixo 'admin' às todas as rotas referentes ao admin */
    ],
    function(){
        //pagina inicial do admin
        Route::get('/', 'AdminController@index')->name('admin.home');

        //pagina de usuarios sem permissao
        Route::get('/unauthorized', 'AdminController@unauthorized')->name('admin.unauthorized');


        //Escolha do Orgao ao logar
        Route::view('/set_orgao', 'admin.users.set_orgao')->name('users.seta_orgao');


        //rotas adicionais
        Route::get('permissions/roles', 'PermissionController@permissionroleindex')->name('permissions.roles.index');
        Route::post('permissions/roles', 'PermissionController@permissionroleupdate')->name('permissions.roles.update');


        //rotas JSON
        Route::get('orgaos/json', 'OrgaoController@json')->name('orgaos.json');
        Route::get('setores/json', 'SetorController@json')->name('setores.json');
        Route::get('roles/json', 'RoleController@json')->name('roles.json');
        Route::get('permissions/json', 'PermissionController@json')->name('permissions.json');
        Route::get('users/json', 'UserController@json')->name('users.json');


        //Rotas para Users/Roles
        Route::post('users/{user_id}/roles/destroy', 'RoleUserController@destroy')->name('users.roles.destroy');
        Route::get('users/{user_id}/roles',  'RoleUserController@index')->name('users.roles.index');
        Route::post('users/{user_id}/roles', 'RoleUserController@store')->name('users.roles.store');



        //Rotas Padrao CRUD
        Route::resources([
            'orgaos'        =>  'OrgaoController',
            'setores'        =>  'SetorController',
            'roles'         =>  'RoleController',
            'permissions'   =>  'PermissionController',
            'users'         =>  'UserController',
        ]);

    }
);



/**
 * Rotas Para o frontend
 */
Route::group(
    [
        'namespace'     =>  'Portal',    /*todos controller estão dentro da pasta (namespace) Portal*/
    ],
    function(){

        //rota de login e logout
        Route::post('/entrar', 'CadastroController@login')->name('portal.login');
        Route::get('/sair', 'CadastroController@logout')->name('portal.logout');


        Route::get('users/json', 'CadastroController@userJson')->name('portal.users.json');


        /*Rotas do cadastro*/
        //rotas get ou post para as etapas do cadastro
        Route::get('/cadastro/{etapa_id}/etapa', 'CadastroController@index')->name('portal.cadastro.etapa1');
        Route::post('/cadastro/{etapa_id}/etapa', 'CadastroController@index')->name('portal.cadastro.etapas');
        Route::put('/cadastro/{etapa_id}/etapa', 'CadastroController@storeBeneficiario')->name('portal.cadastro.store');
        //redireciona o usuario para a etapa 1 do cadastro
        Route::redirect('/cadastro', '/cadastro/1/etapa', 301);

        /*Rotas de reinicialização de senha*/
        //rotas get ou post para as etapas do senha
        Route::get( '/senha/{etapa_id}/etapa', 'CadastroController@senha')->name('portal.senha.etapa1');
        Route::post('/senha/{etapa_id}/etapa', 'CadastroController@senha')->name('portal.senha.etapas');
        Route::put( '/senha/{etapa_id}/etapa', 'CadastroController@storeNovaSenha')->name('portal.senha.store');
        //redireciona o usuario para a etapa 1 do cadastro
        Route::redirect('/senha', '/senha/1/etapa', 301);


        //pagina inicial do admin
        Route::get('/', 'PortalController@index')->name('portal.home');
    }
);

/**
 * Rotas Para o frontend [AUTENTICADO]
 */
Route::group(
    [
        'middleware'    =>  ['auth'],   /*requer autenticacao para todos os controles*/
        'namespace'     =>  'Portal',    /*todos controller estão dentro da pasta (namespace) Admin*/
        'prefix'        =>  'painel'     /*Atribui o prefixo 'admin' às todas as rotas referentes ao admin */
    ],
    function(){
        //pagina inicial do painel
        Route::get('/', 'PainelController@index')->name('portal.painel');

        //dados do usuario
        Route::get('/dados', 'CadastroController@cadastro')->name('portal.painel.dados');
        Route::post('/dados', 'CadastroController@cadastroUpdate')->name('portal.painel.dados.update');

        //pagina de usuarios sem permissao
        Route::get('/unauthorized', 'CadastroController@unauthorized')->name('portal.unauthorized');

    }
);





Auth::routes();
