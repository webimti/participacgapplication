{!! Form::open(['route' => $rota]) !!}

<div class="form-group">
    <label>Data de Nascimento</label>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        {!! Form::text('nascimento',null,['id' => 'nascimento','class' => 'form-control','required'=>true]) !!}
    </div>
</div>

<div class="form-group">
    <label>Sexo</label>
    <div class="input-group" style="background-color: white">
        <div class="input-group-addon"><i class="fa fa-venus-mars"></i></div>
        <select name="sexo" id="sexo" class="form-control" style="border-radius: 0px;" required>
            <option value="0">Selecione</option>
            <option value="m">Masculino</option>
            <option value="f">Feminino</option>
        </select>
    </div>

</div>

<button id="btn_prosseguir" class="btn btn-success btn-sm hide" type="submit">Proseguir >></button>
{!! Form::close() !!}


@section('js')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <link rel="stylesheet" href="{!! asset('vendor/adminlte/plugins/daterangepicker-master/daterangepicker.css') !!}">
    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/daterangepicker-master/moment.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/adminlte/plugins/daterangepicker-master/daterangepicker.js') !!}"></script>

    <script>
        function validaCampos(nascimento){
            var sexo = $('#sexo').val();

            if(nascimento!=null && sexo != "0" ){
                var data_atual = moment();
                //se o intervalo de data é pelo menos maior que 6 meses
                if(data_atual.diff(nascimento,'months') >= 6){
                    //mostra o botão prosseguir
                    $('#btn_prosseguir').removeClass('hide');
                    $('#msg_erro').html('');
                }else{
                    $('#msg_erro').html('É preciso ter pelo menos 6 meses de idade para poder realizar o cadastro.');
                    $('#btn_prosseguir').addClass('hide');
                }
            }else{
                $('#msg_erro').html('É preciso ter pelo menos 6 meses de idade para poder realizar o cadastro.<br>É preciso selecionar o sexo.');
                $('#btn_prosseguir').addClass('hide');
            }
        }

        $(function () {

            $('#sexo').on('change',function(){
                validaCampos(moment([$('#nascimento').val()]));
            });

            //var linkEdit = '{{route('orgaos.edit',999)}}'; /***config***/
            $('#nascimento').daterangepicker({
                "showDropdowns": true,
                "singleDatePicker": true,
                "format": 'YYYY-MM-DD',
                "locale": datepickrangerLocale
            }, function(start,b,c) {
                validaCampos(moment(start.format('YYYY-MM-DD')));
            });

        });
    </script>
@stop
