<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserAdmin;
use App\Models\Role;
use App\Models\Role_User;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;



class CadastroController extends Controller
{
    public function index(Request $request, $etapa_id)
    {
        switch ($etapa_id){
            case 1: return view('portal.cadastro.index',['overlay'=>['title'=>'Cadastro de Cidadão','view'=>'portal.cadastro.etapa1'],'rota'=>['portal.cadastro.etapas',2]]);
            case 2:
                //se passou nascimento
                if($request->filled('nascimento') && $request->filled('sexo')){
                    //caclula a diferença em meses entre as datas atual e nascimento
                    $datetime1 = date_create($request->input('nascimento'));
                    $datetime2 = date_create("now");
                    $interval = date_diff($datetime1, $datetime2);
                   //se da dirença for
                    if( (($interval->y*12)+$interval->m) >= config('parametrizacao.frontend_cadastro_idade_minima_meses')){
                        return view('portal.cadastro.index',['overlay'=>['title'=>'Cadastro de Cidadão','view'=>'portal.cadastro.etapa2'],'nascimento'=>$request->input('nascimento'),'sexo'=>$request->input('sexo'),'rota'=>['portal.cadastro.etapas',3]]);
                    }

                }
                //gera o erro
                return redirect()->route('portal.cadastro.etapa1',1) ->withErrors(["nascimento"=>"O cidadão deve informar uma data de nascimento e deve possuir pelo menos 6 meses de vida.<br>É preciso selecionar o sexo."]);
            case 3:
                //verifica se passou os campos nome e nascimento
                if($request->filled('sexo') && $request->filled('nascimento') && $request->filled('nome_completo')){
                    //se selecionou algum usuário já existente
                    if( intval($request->input('nome_completo')) != 0){
                        /*se passou um usuário já existente, redireciona para login com mensagem de erro*/
                        return view('portal.home.index',['overlay'=>['title'=>'Atenção!','html'=>'Já existe um usuário com este mesmo nome e data de nascimento. Por favor, faça o login.','close'=>true]]);
                    }
                    $variaveis = ['nascimento'=>$request->input('nascimento'),'sexo'=>$request->input('sexo'),'nome_completo'=>$request->input('nome_completo')];

                    //calcula a diferença em anos entre as datas atual e nascimento, para ver se necessita responsavel
                    $datetime1 = date_create($request->input('nascimento'));
                    $datetime2 = date_create("now");
                    $interval = date_diff($datetime1, $datetime2);
                    if( $interval->y < config('parametrizacao.frontend_cadastro_idade_minima_responsavel')){
                        $variaveis['responsavel']=1;
                    }

                    $variaveis['rota'] = ['portal.cadastro.etapas',3] ;

                    return view('portal.cadastro.etapa3',$variaveis);
                }
        }


    }

    /**
     * Retorna um objeto datatables
     * @param Request $request
     * @return mixed
     */
    public function userJson(Request $request)
    {
        $datatablejson = new UserAdmin();
        return $datatablejson->dataTable($request);
    }



    public function storeBeneficiario(Request $request)
    {
        /*se passou um usuário já existente, redireciona para login com mensagem de erro*/
        if( intval($request->input('name')) != 0){
            return view('portal.home.index',['overlay'=>['title'=>'Atenção!','html'=>'Já existe um usuário com este mesmo nome e data de nascimento. Por favor, faça o login.','close'=>true]]);
        }

        //variaveis a serem passadas para a view
        $variaveis = [];

        //calcula a diferença em anos entre as datas atual e nascimento, para ver se necessita responsavel
        $datetime1 = date_create($request->input('nascimento'));
        $datetime2 = date_create("now");
        $interval = date_diff($datetime1, $datetime2);
        if( $interval->y < config('parametrizacao.frontend_cadastro_idade_minima_responsavel')){
            $variaveis['responsavel']=1;
        }else{
            $variaveis['responsavel']=2;
        }


        //agrupa os campos enviados pelo request
        $fieldsFromRequest = $request->all();



        $validadores =  [
            'name' =>       'required|min:5|max:255',
            'nascimento' => 'required|date',
            'sexo' =>       'required',
            'saude' =>      'max:255',

            'celular' =>    'required_without:outrofone',
            'outrofone' =>  'required_without:celular',

            'cpf' =>        'required|min:11|max:50|unique:users,cpf',
            'senha' =>      'required|confirmed|min:6',

        ];


        if($request->filled('email')) {
            $validadores['email'] = 'email|unique:users,email';
        }
        if($request->filled('celular')) {
            $fieldsFromRequest['celular'] = str_replace('_', '', $fieldsFromRequest['celular']);
        }
        if($request->filled('outrofone')) {
            $fieldsFromRequest['outrofone'] = str_replace('_', '', $fieldsFromRequest['outrofone']);
        }
        if($request->filled('responsavel_fone')) {
            $fieldsFromRequest['responsavel_fone'] = str_replace('_', '', $fieldsFromRequest['responsavel_fone']);
            $validadores['responsavel_fone'] = 'required|min:13|max:15';
        }
        if($request->filled('responsavel_nome')) {
            $validadores['responsavel_nome'] = 'required|min:5|max:255';
        }
        if($request->filled('responsavel_cpf')) {
            $validadores['responsavel_cpf'] = 'required|min:11|max:50';
        }



        //valida os campos enviados
        $validator = Validator::make($fieldsFromRequest,$validadores);

        //se falhou validação, envia novamente para o formulário
        if ($validator->fails()) {
            return view('portal.cadastro.etapa3')->withErrors($validator)->with($fieldsFromRequest)->with($variaveis);
        }

        //cria uma instancia do Usuário
        $novoUsuario = new UserAdmin();
        //atribui todos os campos
        $novoUsuario->name = $fieldsFromRequest['name'];
        $novoUsuario->email = $fieldsFromRequest['email'];
        $novoUsuario->password = Hash::make($fieldsFromRequest['senha']);
        $novoUsuario->nascimento = $fieldsFromRequest['nascimento'];
        $novoUsuario->cpf = $fieldsFromRequest['cpf'];
        if($request->filled('celular'))
            $novoUsuario->celular = $fieldsFromRequest['celular'];
        if($request->filled('outrofone'))
            $novoUsuario->outrofone = $fieldsFromRequest['outrofone'];
        $novoUsuario->sexo = $fieldsFromRequest['sexo'];
        if($request->filled('saude'))
            $novoUsuario->saude = $fieldsFromRequest['saude'];
        if($request->filled('responsavel_nome')) {
            $novoUsuario->responsavel_nome = $fieldsFromRequest['responsavel_nome'];
            $novoUsuario->responsavel_fone = $fieldsFromRequest['responsavel_fone'];
            $novoUsuario->responsavel_cpf = $fieldsFromRequest['responsavel_cpf'];
        }

        //salva e insere no banco o novo usuário
        if($novoUsuario->save()){//se inseriu com sucesso
            //recupera o id do papel GUEST
            $role_guest_id = Role::where('name','GUEST')->get()->first()->id;
            //Atribui o papel GUEST ao novo usuário
            Role_User::firstOrCreate(['user_id'=>$novoUsuario->id,'role_id'=>$role_guest_id]);
            return view('portal.home.index',['overlay'=>['title'=>'Cadastrado com Sucesso','html'=>'Você concluiu seu cadastro com sucesso. A partir de agora você pode usufruir dos programas, ações e eventos da prefeitura.','close'=>true]]);
        }

        //se houve erro no cadastro
        return view('portal.cadastro.etapa3',['overlay'=>['title'=>'Erro ao gravar dados','html'=>'Tente novamente ou entre em contato com a central de atendimento através do telefone '.config('parametrizacao.fone_central_atendimento'),'close'=>true]])->with($fieldsFromRequest)->with($variaveis);

    }




    /**
     * Controla o processo de reinicialização da senha
     */
    public function senha(Request $request, $etapa_id)
    {
        switch ($etapa_id){
            case 1: return view('portal.cadastro.index',['overlay'=>['title'=>'Reiniciar Senha','html'=>'Confirme seus dados para prosseguir','view'=>'portal.cadastro.etapa1'],'rota'=>['portal.senha.etapas',2]]);
            case 2:
                //se passou nascimento
                if($request->filled('nascimento') && $request->filled('sexo')){
                    return view('portal.cadastro.index',['overlay'=>['title'=>'Reiniciar Senha','view'=>'portal.cadastro.etapa2'],'nascimento'=>$request->input('nascimento'),'sexo'=>$request->input('sexo'),'rota'=>['portal.senha.etapas',3]]);
                }
                //gera o erro
                return redirect()->route('portal.senha.etapa1',1) ->withErrors(["nascimento"=>"O cidadão deve informar uma data de nascimento.<br>É preciso selecionar o sexo."]);
        }


    }


    public function storeNovaSenha(Request $request)
    {
        //variaveis a serem passadas para a view
        $variaveis = [];

        //agrupa os campos enviados pelo request
        $fieldsFromRequest = $request->all();


        //Seleciona o usuário
        if(intval($request->filled('name'))!=0) {
            $novoUsuario = UserAdmin::find($request->filled('name'));
        }
        else {
            $novoUsuario = UserAdmin::find($request->filled('id'));
        }



        $fieldsFromRequest['name'] = $novoUsuario->name;

        $validadores = [
            'name' =>       'required|min:5|max:255|exists:users',
            'nascimento' => 'required|date|exists:users',
            'sexo' =>       'required|exists:users',
            'cpf' =>        'required|min:11|max:50|exists:users',
            'senha' =>      'confirmed|min:6',
        ];

        //se tem campo responsavel
        if($request->filled('responsavel_cpf')) {
            $validadores['$validadores'] =  'required|min:11|max:50|exists:users';
        }

        //se tem campo email
        if($request->filled('email')) {
            $validadores['email'] =  'required|email|exists:users';
        }

        //valida os campos enviados
        $validator = Validator::make($fieldsFromRequest, $validadores);

        //se falhou validação, envia novamente para o formulário
        if ($validator->fails()) {
            return view('portal.cadastro.senha_etapa3',['usuario'=>$novoUsuario->toArray()])->withErrors($validator);
        }


        //atribui todos os campos
        $novoUsuario->password = Hash::make($fieldsFromRequest['senha']);

        //salva no banco a nova senha
        if($novoUsuario->save()){//se inseriu com sucesso
            return view('portal.home.index',['overlay'=>['title'=>'Senha Alterada','html'=>'Você alterou sua senha com sucesso.','close'=>true]]);
        }

        //se houve erro no cadastro
        return view('portal.cadastro.senha_etapa3',['overlay'=>['title'=>'Erro ao gravar dados','html'=>'Tente novamente ou entre em contato com a central de atendimento através do telefone '.config('parametrizacao.fone_central_atendimento'),'close'=>true],'usuario'=>$fieldsFromRequest]);

    }



    public function login(Request $request)
    {
        /*echo Hash::make($request->input('senha'));
        print_r($request->all());
        echo $request->input('senha');
        exit;*/
        if (Auth::attempt(['cpf' => $request->input('cpf'), 'password' => $request->input('senha')])) {
            // Authentication passed...
            return redirect()->intended('/');
        }
        return view('portal.home.index',['overlay'=>['title'=>'Erro de Login','html'=>'Usuário e senha informados não correspondem.','close'=>true],'nascimento'=>$request->input('nascimento'),'sexo'=>$request->input('sexo'),'rota'=>['portal.senha.etapas',3]]);

    }


    public function logout()
    {
        Auth::logout();
        return redirect()->route('portal.home') ;


    }

    public function cadastro()
    {
        return view('portal.painel.dados',['usuario'=>UserAdmin::find(Auth::user()->id)]);
    }

    public function cadastroUpdate(Request $request)
    {
        //recupera o usuário atual
        $novoUsuario = UserAdmin::find(Auth::user()->id);

        //agrupa os campos enviados pelo request
        $fieldsFromRequest = $request->all();


        $validadores =  [
            'celular' =>    'required_without:outrofone',
            'outrofone' =>  'required_without:celular',
        ];


        if($request->filled('saude')) {
            $validadores['saude'] = 'max:255';
        }
        if($request->filled('email')) {
            $validadores['email'] = 'email';
        }
        if($request->filled('senha')) {
            $validadores['senha'] = 'required|confirmed|min:6';
        }
        if($request->filled('celular')) {
            $fieldsFromRequest['celular'] = str_replace('_', '', $fieldsFromRequest['celular']);
        }
        if($request->filled('outrofone')) {
            $fieldsFromRequest['outrofone'] = str_replace('_', '', $fieldsFromRequest['outrofone']);
        }
        if($request->filled('responsavel_fone')) {
            $fieldsFromRequest['responsavel_fone'] = str_replace('_', '', $fieldsFromRequest['responsavel_fone']);
            $validadores['responsavel_fone'] = 'required|min:13|max:15';
        }
        if($request->filled('responsavel_nome')) {
            $validadores['responsavel_nome'] = 'required|min:5|max:255';
        }
        if($request->filled('responsavel_cpf')) {
            $validadores['responsavel_cpf'] = 'required|min:11|max:50';
        }



        //valida os campos enviados
        $validator = Validator::make($fieldsFromRequest,$validadores);

        //se falhou validação, envia novamente para o formulário
        if ($validator->fails()) {
            return view('portal.painel.dados',['usuario'=>$novoUsuario])->withErrors($validator)->with($fieldsFromRequest);
        }

        //atribui todos os campos
        if($request->filled('celular'))
            $novoUsuario->celular = $fieldsFromRequest['celular'];
        if($request->filled('outrofone'))
            $novoUsuario->outrofone = $fieldsFromRequest['outrofone'];
        if($request->filled('saude'))
            $novoUsuario->saude = $fieldsFromRequest['saude'];
        if($request->filled('responsavel_nome')) {
            $novoUsuario->responsavel_nome = $fieldsFromRequest['responsavel_nome'];
            $novoUsuario->responsavel_fone = $fieldsFromRequest['responsavel_fone'];
            $novoUsuario->responsavel_cpf = $fieldsFromRequest['responsavel_cpf'];
        }
        if($request->filled('email'))
            $novoUsuario->email = $fieldsFromRequest['email'];
        if($request->filled('senha'))
            $novoUsuario->password = Hash::make($fieldsFromRequest['senha']);


        //salva no banco as alterações
        if($novoUsuario->save()){//se salvou com sucesso
            return view('portal.painel.dados',['usuario'=>$novoUsuario, 'overlay'=>['title'=>'Dados alterados com sucesso','close'=>true]]);
        }

        //se houve erro no cadastro
        return view('portal.painel.dados',['usuario'=>$novoUsuario,'overlay'=>['title'=>'Erro ao gravar dados','html'=>'Tente novamente ou entre em contato com a central de atendimento através do telefone '.config('parametrizacao.fone_central_atendimento'),'close'=>true]]);

    }

}
