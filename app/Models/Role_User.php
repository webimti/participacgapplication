<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Orgao;
use App\Models\UserAdmin;

class Role_User extends Model
{
    protected $fillable = ['user_id','role_id','orgao_id'];

    protected $table = 'role_user';

    /**
     * Retorna todas as permissões de um usuário
     */
    public static function roles(UserAdmin $user){
        return Role_User::query()
            ->join('users','users.id', 'role_user.user_id')
            ->join('roles','roles.id', 'role_user.role_id')
            ->leftJoin('orgaos','orgaos.id', 'role_user.orgao_id')
            ->select('role_user.id','role_user.role_id','roles.name','role_user.orgao_id','orgaos.sigla','orgaos.ativo','users.id as user')

            ->where('role_user.user_id', $user->id) //somente do usuario atual
            ->where('roles.ativo', 1) //somente papeis ativos
            ->whereRaw('((role_user.orgao_id IS NOT NULL AND orgaos.ativo is true) OR (role_user.orgao_id IS NULL))') //somente orgaos ativos, se

            ->orderBy('roles.name')
            ->get()->toArray();
    }

}
