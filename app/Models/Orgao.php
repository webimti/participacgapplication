<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orgao extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sigla','name','ativo'];


    /**
     * Retorna todas orgaos habilitados
     */
    public static function habilitadosQuery(){
        return Orgao::query()->where('ativo',1)->orderBy('sigla');
    }

    /**
     * Retorna todas orgaos habilitados
     */
    public static function habilitados(){
        return Orgao::habilitadosQuery()->get();
    }
}
