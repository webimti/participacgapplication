<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDneGuRegioesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dne_gu_regioes', function (Blueprint $table) {
            $table->integer('chave_reg')->unsigned();
            $table->string('nome_ofi_reg',40);

            $table->primary('chave_reg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dne_gu_regioes');
    }
}
