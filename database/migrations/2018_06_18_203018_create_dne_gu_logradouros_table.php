<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDneGuLogradourosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dne_gu_logradouros', function (Blueprint $table) {
            $table->integer('chave_logradouro_dne')->unsigned();
            $table->integer('chave_loc_dne')->unsigned();

            $table->string('sigla_uf',2)->nullable();

            $table->integer('chave_bai_ini_dne')->unsigned()->nullable();
            $table->integer('chave_bai_fim_dne')->unsigned()->nullable();
            $table->string('tipo_ofi_logradouro',26)->nullable();
            $table->string('preposicao',3)->nullable();
            $table->string('tit_pat_ofi_logradouro',72)->nullable();
            $table->string('nome_ofi_logradouro',72)->nullable();
            $table->string('abrev_log_rec_ect',36)->nullable();
            $table->string('info_adicional',130)->nullable();
            $table->string('cep_logradouro',8)->nullable();
            $table->string('ind_exis_gu_log',1)->nullable();
            $table->string('informa',200)->nullable();
            $table->string('numini',11)->nullable();
            $table->string('numfim',11)->nullable();
            $table->string('tiponum',1)->nullable();
            $table->string('nome_completo_ofi',100)->nullable();

            $table->primary('chave_logradouro_dne');

            $table->foreign('chave_loc_dne')
                ->references('chave_loc_dne')
                ->on('dne_gu_localidades')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dne_gu_logradouros');
    }
}
