@extends('adminlte::page')


@section('content_header')
    <h1>Permissões</h1>
@stop

@section('content')
    <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <a href="{{route('permissions.create')}}" class="btn btn-success"><i class="fa fa-plus-square"></i> &nbsp; &nbsp;Criar Novo</a>
                </h3>

            </div>
    <!-- /.box-header -->
        <div class="box-body">
            <table id="datatableGrid" class="table table-bordered table-striped">
                <thead>
                <tr id="searchHeader">
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <th>ID</th>
                    <th>NOME</th>
                    <th>DESCRIÇÃO</th>
                    <th>ATIVO</th>
                    <th>#</th>
                </tr>

                </thead>
                <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                </tbody>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>NOME</th>
                    <th>DESCRIÇÃO</th>
                    <th>ATIVO</th>
                    <th>#</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <script>

    </script>
@stop

@section('js')

    <script type="text/javascript" src="{!! asset('js/participacg.js') !!}"></script>

    <script>
        $(function () {

            var linkEdit = '{{route('permissions.edit',999)}}'; /***config***/

            var table = $('#datatableGrid').DataTable({
                "dom": 'lrtip',
                "serverSide": true,
                "processing": true,
                "pageLength": 25,
                "lengthMenu": [[25, 50, 100], [25, 50, 100]],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
                },
                ajax: {
                    url: '{{route('permissions.json')}}',  /***config***/
                    dataSrc: 'data',
                    "dataType": "json",
                    "type": "GET",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                    /***config***/
                columns: [
                    { data: 'id'    },
                    { data: 'name'  },
                    { data: 'label'  },
                    { data: 'ativo', render:function(data){return (data==1)?'SIM':'NÃO';} },
                    { data: 'id', render:function(data){return  '<a class="btn btn-small btn-primary" href="'+linkEdit.split('999').join(data)+'"><i class="fa fa-edit"></i></a>';} },

                ],
                    /***config***/
                columnDefs: [
                    { targets: [ 0],     type: "number",     searchable: true,     sortable: true},
                    { targets: [ 1],     type: "string",     searchable: true,     sortable: true},
                    { targets: [ 2],     type: "string",     searchable: true,     sortable: true},
                    { targets: [ 3],     type: "boolean",    searchable: true,     sortable: true},
                    { targets: [ 4],     type: "null",       searchable: false,    sortable: false}
                ],
            });

            //oculta o box search
            $(".dataTables_filter").hide();

            datatablesGeraCamposPesquisaveis(table);

        });
    </script>
@stop
